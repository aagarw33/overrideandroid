package com.magician.actionbar.searchactionbardemo;

import java.util.ArrayList;
import android.annotation.TargetApi;
import android.app.ListFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class ActionBarSearchFragment extends ListFragment implements
		TextView.OnEditorActionListener, SearchView.OnQueryTextListener,
		SearchView.OnCloseListener {

	private static final String[] items = { "lorem", "ipsum", "dolor", "sit",
			"amet", "consectetuer", "adipiscing", "elit", "morbi", "vel",
			"ligula", "vitae", "arcu", "aliquet", "mollis", "etiam", "vel",
			"erat", "placerat", "ante", "porttitor", "sodales", "pellentesque",
			"augue", "purus" };
	private ArrayList<String> words = null;
	private ArrayAdapter<String> adapter = null;
	private static final String STATE_QUERY = "q";
	private static final String STATE_MODEL = "m";
	private CharSequence initalQuery = null;
	private SearchView sv = null;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
		setHasOptionsMenu(true);

		if (savedInstanceState == null) {
			initAdapter(null);
		} else {
			initAdapter(savedInstanceState.getStringArrayList(STATE_MODEL));
			initalQuery = savedInstanceState.getCharSequence(STATE_QUERY);
		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (!sv.isIconified()) {
			outState.putCharSequence(STATE_QUERY, sv.getQuery());
		}
		outState.putStringArrayList(STATE_MODEL, words);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.main, menu);

		configureSearchView(menu);

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Toast.makeText(getActivity(), adapter.getItem(position),
				Toast.LENGTH_LONG).show();
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
			adapter.add(v.getText().toString());
			v.setText("");

			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		}

		return (true);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	/*
	 * Register our Fragment as QueryTextListener and OnCloseListener
	 * Disable submit button, as we will be using SearchView for filtering rather than querying
	 * Indicate that SearchView should be collapsed("iconified") as default state.
	 */
	private void configureSearchView(Menu menu) {
		MenuItem search = menu.findItem(R.id.search);

		sv = (SearchView) search.getActionView();
		sv.setOnQueryTextListener(this);
		sv.setOnCloseListener(this);
		sv.setSubmitButtonEnabled(false);
		sv.setIconifiedByDefault(true);

		if (initalQuery != null) {
			sv.setIconified(false);
			search.expandActionView();
			sv.setQuery(initalQuery, true);
		}
	}

	private void initAdapter(ArrayList<String> startPoint) {
		if (startPoint == null) {
			words = new ArrayList<String>();

			for (String s : items) {
				words.add(s);
			}
		} else {
			words = startPoint;
		}
		adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, words);

		setListAdapter(adapter);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.SearchView.OnCloseListener#onClose()
	 * 
	 * required by SearchView.OnCloseListener, in theory is called when SearchView is collapsed, 
	 * According to the SearchView source code it is called when query text is empty and SearchView is iconified by default.
	 */
	@Override
	public boolean onClose() {
		adapter.getFilter().filter("");
		return (true);
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.SearchView.OnQueryTextListener#onQueryTextSubmit(java.lang.String)
	 * 
	 * required by SearchView.OnQueryTextListener will be called if user tapped on submit 
	 * button within the expanded searchview.
	 * 
	 * Disabling this button as we filtering the list and not querying it.
	 */
	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.SearchView.OnQueryTextListener#onQueryTextChange(java.lang.String)
	 * 
	 * required by SearchView.OnQueryTextListener will be called whenever the use has changed
  	 * the contents of the expanded SearchView. This is used when you want to employ the SearchView for filtering,
  	 * updating the filter.
  	 * 
  	 * Simply updates the filter with whatever the user has typed into the searchview.
	 */
	@Override
	public boolean onQueryTextChange(String newText) {
		if (TextUtils.isEmpty(newText)) {
			adapter.getFilter().filter("");
		} else {
			adapter.getFilter().filter(newText.toString());
		}
		return (true);
	}
}
