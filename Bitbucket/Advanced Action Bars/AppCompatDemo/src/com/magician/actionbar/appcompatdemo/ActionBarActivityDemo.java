package com.magician.actionbar.appcompatdemo;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ActionBarActivityDemo extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		findViewById(android.R.id.content).post(new Runnable() {
			
			@Override
			public void run() {
				if(getSupportFragmentManager().findFragmentById(R.id.fragment) == null){
					getSupportFragmentManager().beginTransaction()
										.add(R.id.fragment, new ActionBarFragmentDemo()).commit();
				}
				
			}
		});
	}

}
