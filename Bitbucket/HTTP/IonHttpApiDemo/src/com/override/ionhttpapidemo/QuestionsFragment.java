package com.override.ionhttpapidemo;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class QuestionsFragment extends
		ContractListFragment<QuestionsFragment.Contract> implements
		FutureCallback<JsonObject> {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View res = super.onCreateView(inflater, container, savedInstanceState);
		setRetainInstance(true);
		Ion.with(
				getActivity(),
				"https://api.stackexchange.com/2.1/questions?"
						+ "order=desc&sort=creation&site=stackoverflow&"
						+ "tagged=android").asJsonObject().setCallback(this);
		return (res);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		getContract().showItem(
				((ItemsAdapter) getListAdapter()).getItem(position));
	}

	public interface Contract {
		void showItem(JsonObject item);
	}

	@Override
	public void onCompleted(Exception e, JsonObject json) {
		// TODO Auto-generated method stub
		if (e !=null){
			Toast.makeText(null, e.getMessage(), Toast.LENGTH_LONG).show();
		}
		
		if(json !=null){
			JsonArray items = json.getAsJsonArray("items");
			ArrayList<JsonObject> normalized = new ArrayList<JsonObject>();
			for(int i =0;i<items.size();i++){
				normalized.add(items.get(i).getAsJsonObject());
			}
			
			setListAdapter(new ItemsAdapter(normalized));
		}
	}
	
	class ItemsAdapter extends ArrayAdapter<JsonObject>{
		int size;
		
		ItemsAdapter(List<JsonObject> items) {
			// TODO Auto-generated constructor stub
			super(getActivity(),R.layout.row,R.id.title,items);
			
			size = 
					getActivity().getResources().getDimensionPixelSize(R.dimen.icon);
		}
	
	
	 @Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	      View row=super.getView(position, convertView, parent);
	      JsonObject item=getItem(position);
	      ImageView icon=(ImageView)row.findViewById(R.id.icon);

	      Ion.with(icon)
	         .placeholder(R.drawable.owner_placeholder)
	         .resize(size, size)
	         .centerCrop()
	         .error(R.drawable.owner_error)
	         .load(item.getAsJsonObject("owner").get("profile_image")
	                   .getAsString());

	      TextView title=(TextView)row.findViewById(R.id.title);

	      title.setText(Html.fromHtml(item.get("title").getAsString()));

	      return(row);
	    }
	  }


}

