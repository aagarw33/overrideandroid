package com.override.ionhttpapidemo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.JsonObject;

public class MainActivity extends Activity implements
		QuestionsFragment.Contract {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getFragmentManager().findFragmentById(android.R.id.content) == null) {
			getFragmentManager().beginTransaction()
					.add(android.R.id.content, new QuestionsFragment())
					.commit();
		}
	}

	@Override
	public void showItem(JsonObject item) {
		Intent iCanHazBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(item
				.get("link").getAsString()));

		startActivity(iCanHazBrowser);
	}
}