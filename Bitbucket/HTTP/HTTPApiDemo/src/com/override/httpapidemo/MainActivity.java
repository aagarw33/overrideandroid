package com.override.httpapidemo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity implements
		QuestionsFragment.Contract {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getFragmentManager().findFragmentById(android.R.id.content) == null) {
			getFragmentManager().beginTransaction()
					.add(android.R.id.content, new QuestionsFragment())
					.commit();
		}
	}

	@Override
	public void showItem(Item item) {
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.link)));
	}
}