package com.override.httpapidemo;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class QuestionsFragment extends
    ContractListFragment<QuestionsFragment.Contract> implements
    Callback<Questions> {
  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container,
                           Bundle savedInstanceState) {
    View result=
        super.onCreateView(inflater, container, savedInstanceState);

    setRetainInstance(true);

    RestAdapter restAdapter=
        new RestAdapter.Builder().setEndpoint("https://api.stackexchange.com")
                                 .build();
    StackOverflowInterface so=
        restAdapter.create(StackOverflowInterface.class);

    so.questions("android", this);

    return(result);
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    getContract().showItem(((ItemsAdapter)getListAdapter()).getItem(position));
  }

  @Override
  public void failure(RetrofitError exception) {
    Toast.makeText(getActivity(), exception.getMessage(),
                   Toast.LENGTH_LONG).show();
    Log.e(getClass().getSimpleName(),
          "Exception from Retrofit request to StackOverflow", exception);
  }

  @Override
  public void success(Questions questions, Response response) {
    setListAdapter(new ItemsAdapter(questions.items));
  }

  class ItemsAdapter extends ArrayAdapter<Item> {
	  int size;
    ItemsAdapter(List<Item> items) {
      super(getActivity(), R.layout.row, R.id.title,items);
      
      size = 
    		  getActivity().getResources().getDimensionPixelSize(R.dimen.icon);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      View row=super.getView(position, convertView, parent);
      Item item = getItem(position);
      ImageView icon = (ImageView)row.findViewById(R.id.icon);
      
      Picasso.with(getActivity()).load(item.owner.profileImage)
      .resize(size, size).centerCrop().placeholder(R.drawable.owner_placeholder)
      .error(R.drawable.owner_error).into(icon);
      
      TextView title=(TextView)row.findViewById(R.id.title);

      title.setText(Html.fromHtml(getItem(position).title));

      return(row);
    }
  }

  interface Contract {
    void showItem(Item item);
  }
}