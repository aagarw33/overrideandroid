package com.override.httpapidemo;

import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.Callback;

public interface StackOverflowInterface {

	@GET("/2.1/questions?order=desc&sort=creation&site=stackoverflow")
	void questions(@Query("tagged") String tags, Callback<Questions> cb);
}
