package com.override.ereader;

import retrofit.http.GET;

/*
 * interface that is utilized by retrofit
 * to convert the JSOn object to usable java object
 * Using HTTP GET operation to retrive the JSOn, in retrofit it is
 * @GET
 */
public interface BookUpdateInterface {

	@GET("/misc/empublite-update.json")
	BookUpdateInfo update();
}
