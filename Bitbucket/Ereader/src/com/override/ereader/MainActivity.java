package com.override.ereader;

import android.app.Activity;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import de.greenrobot.event.EventBus;

public class MainActivity extends Activity implements OnBackStackChangedListener {
	private ViewPager _pager = null;
	private BookModelFragment _bfrag = null;
	private ContentFragmentAdapter _adapt = null;
	private static final String _KEEP_SCREEN_ON = "keepScreenOn";// key to keep
																	// the
																	// screen
																	// on.
	private static final String _BOOK = "model";
	private static final String _LAST_POSITION = "lastPosition";// key to save
																// last read
																// position in
																// shared
																// preference
	private static final String _SAVED_POSITION = "saveLastPosition";// key to
																		// restore
																		// the
																		// last
																		// read
																		// position.
	private View sidebar = null;
	private View divider = null;
	private ContentFragment help = null;
	private ContentFragment about = null;
	private static final String HELP = "help";
	private static final String ABOUT = "about";
	private static final String FILE_HELP ="file:///android_asset/misc/help.html";
	private static final String FILE_ABOUT="file:///android_asset/misc/about.html";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		_pager = (ViewPager) findViewById(R.id.pager);
		sidebar = findViewById(R.id.sidebar);
		divider = findViewById(R.id.divider);

		/*
		 * Checking if we already have an instance of BookFragment before
		 * creating a new one
		 */

		getFragmentManager().addOnBackStackChangedListener(this);
		if(getFragmentManager().getBackStackEntryCount()>0){
			openSidebar();
		}
		_bfrag = (BookModelFragment) getFragmentManager().findFragmentByTag(
				_BOOK);

		if (_bfrag == null) {
			_bfrag = new BookModelFragment();
			getFragmentManager().beginTransaction().add(_bfrag, _BOOK).commit();
		} else {
			setupPager(_bfrag.getBook());
		}
		
		help =
			(ContentFragment)getFragmentManager().findFragmentByTag(HELP);
		about = 
			(ContentFragment)getFragmentManager().findFragmentByTag(ABOUT);

		getActionBar().setHomeButtonEnabled(true);
		UpdateReceiver.scheduleAlarm(this);
	}

/*
 * Here getting the existing linear layout parameters from the siderbar.
 * if it is 0 assigning weight = 3 and updating it via setLayoutParams()
 * toggling the visiblity of divider.
 */
	private void openSidebar() {
		LinearLayout.LayoutParams p = (LinearLayout.LayoutParams) sidebar
				.getLayoutParams();
		//giving the sidebar 30% of the screen 
		if (p.weight == 0) {
			p.weight = 3;
			sidebar.setLayoutParams(p);
		}
		//making the divider visible.
		divider.setVisibility(View.VISIBLE);
	}

	/*
	 * Check to see if sidebar is null
	 * if we have it then call openSiderbar()
	 * to ensure the user can see the sidebar.
	 * create our fragment if we do no have it.
	 * using FragmentTransaction replace to replace
	 * whatever was there in the fragment.
	 * If we do not have the sidebar launch the activity
	 * with appropriate configured intent.
	 * 
	 * addToBackStack = null, so if user press BACK, android
	 * will reverse this transaction.
	 */
	
	private void showAbout(){
		if(sidebar!=null){
			openSidebar();
			if(about == null){
				about = ContentFragment.newInstance(FILE_ABOUT);
			}
			getFragmentManager().beginTransaction().addToBackStack(null)
			.replace(R.id.sidebar, about).commit();
		}else{
			Intent i = new Intent(this,ContentActivity.class);
			i.putExtra(ContentActivity.EXTRA_FILE, FILE_ABOUT);
			startActivity(i);
		}
	}
	/*
	 * Check to see if sidebar is null
	 * if we have it then call openSiderbar()
	 * to ensure the user can see the sidebar.
	 * create our fragment if we do no have it.
	 * using FragmentTransaction replace to replace
	 * whatever was there in the fragment.
	 * If we do not have the sidebar launch the activity
	 * with appropriate configured intent.
	 * 
	 * addToBackStack = null, so if user press BACK, android
	 * will reverse this transaction.
	 */
	private void showHelp(){
		if(sidebar!=null){
			openSidebar();
			if(help == null){
				help = ContentFragment.newInstance(FILE_HELP);
			}
			getFragmentManager().beginTransaction().addToBackStack(null)
			.replace(R.id.sidebar, help).commit();
		}else{
			Intent i = new Intent(this,ContentActivity.class);
			i.putExtra(ContentActivity.EXTRA_FILE, FILE_HELP);
			startActivity(i);
		}
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		EventBus.getDefault().register(this);

		if (_bfrag.getPrefs() != null) {
			_pager.setKeepScreenOn(_bfrag.getPrefs().getBoolean(
					_KEEP_SCREEN_ON, false));
		}
	}

	/*
	 * This will check if the book is loaded. If the book is loaded it will post
	 * it to the main application thread.
	 */

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		EventBus.getDefault().unregister(this);
		if (_bfrag.getPrefs() != null) {
			int position = _pager.getCurrentItem();
			_bfrag.getPrefs().edit().putInt(_LAST_POSITION, position).commit();
		}
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.options, menu);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			_pager.setCurrentItem(0, false);
			return (true);
		case R.id.iabout:
			showAbout();
			return (true);
		case R.id.help:
			showHelp();
			return (true);
		case R.id.settings:
			startActivity(new Intent(this, Preference.class));
			return (true);

		case R.id.notes:
			Intent i = new Intent(this, NotesActivity.class);
			i.putExtra(NotesActivity.EXTRA_POSITION, _pager.getCurrentItem());
			startActivity(i);
			return (true);
		case R.id.update:
			/*
			 * using third party library to schedule the wake full work if the
			 * user is not using the device, and the alarm goes off the work
			 * mite be skipped and device will fall asleep while we try to
			 * download the update this library takes care of the work.
			 */
			WakefulIntentService.sendWakefulWork(this,
					DownloadCheckService.class);
			return (true);
		}
		return (super.onOptionsItemSelected(item));
	}

	public void onEventMainThread(BookLoadEvent event) {
		setupPager(event.getBook());
	}

	private void setupPager(BookContents cont) {
		_adapt = new ContentFragmentAdapter(this, cont);
		_pager.setAdapter(_adapt);
		findViewById(R.id.progressBar1).setVisibility(View.GONE);
		findViewById(R.id.pager).setVisibility(View.VISIBLE);

		SharedPreferences _prefs = _bfrag.getPrefs();

		if (_prefs != null) {
			if (_prefs.getBoolean(_SAVED_POSITION, false)) {
				_pager.setCurrentItem(_prefs.getInt(_LAST_POSITION, 0));
			}
			_pager.setKeepScreenOn(_prefs.getBoolean(_KEEP_SCREEN_ON, false));

		}
	}

	@Override
	public void onBackStackChanged() {
		if(getFragmentManager().getBackStackEntryCount() == 0){
			LinearLayout.LayoutParams p = 
					(LinearLayout.LayoutParams)sidebar.getLayoutParams();
			if(p.weight>0){
				p.weight=0;
				sidebar.setLayoutParams(p);
				divider.setVisibility(View.GONE);
			}
		}
		
	}
}
