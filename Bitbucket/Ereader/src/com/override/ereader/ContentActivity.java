package com.override.ereader;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

public class ContentActivity extends Activity {

	private static final String TAG = "ContentActivity";
	public static final String EXTRA_FILE = "file";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		if
		(getFragmentManager().findFragmentById(android.R.id.content)==null){
			String _file = getIntent().getStringExtra(EXTRA_FILE);
			Fragment _frag = ContentFragment.newInstance(_file);
			
			getFragmentManager().beginTransaction().add(android.R.id.content,
														_frag).commit();
		}
	}
	
}
