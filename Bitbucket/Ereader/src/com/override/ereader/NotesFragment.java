package com.override.ereader;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ShareActionProvider;
import de.greenrobot.event.EventBus;

public class NotesFragment extends Fragment implements TextWatcher {

	/*
	 * tapping on the action bar items keeps NoteFragemnt on the screen.
	 * We might want to come back to the book instead, Notefragment cannot do it automatically,
	 * We can create another object in the event bus to do that, but creating an 
	 * Interface to demonstrate contract pattern which does this feature.
	 */
	public interface contract{
		void closeNotes();
	}
	
	private ShareActionProvider share = null;
	private Intent shareIntent = new Intent(Intent.ACTION_SEND).setType("text/plain");
	private static final String KEY_POSITION = "position";
	private EditText note = null;
	
	/*
	 * static factory method, that creates an instance of NoteFragment
	 * identifies the chapter for which we are creating the note.
	 * puts the integer value in the bundle.
	 * And hands the bundle as an argument to the fragment.
	 */
	static NotesFragment newInstance(int position){
		NotesFragment frag = new NotesFragment();
		Bundle arg = new Bundle();
		arg.putInt(KEY_POSITION, position);
		frag.setArguments(arg);
		return(frag);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View _r = inflater.inflate(R.layout.notes, container,false);
		int position = getArguments().getInt(KEY_POSITION,-1);
		
		note = (EditText)_r.findViewById(R.id.editor);
		note.addTextChangedListener(this);//informs when the user changed the text.
		
		return(_r);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Fragment#onResume()
	 * 
	 * to add the note to the fragment,
	 * we do not have to query the database.
	 * we do not even have to call loadNote() from DatabaseHelper class
	 * but be able to respond to the NotesLoadEvent when it arrives.
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		EventBus.getDefault().register(this); //resgistering the eventbus
		
		/*
		 * checking if there is text in the editext
		 * if exists calling the singleton instance of DatabaseHelper, passing the position
		 * that our fragment is managing and calling loadNote method
		 */
		if(TextUtils.isEmpty(note.getText())){
			DatabaseHelper _db = DatabaseHelper.getInstance(getActivity());
			_db.loadNote(getPosition());
		}
		
	}
	


	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		DatabaseHelper.getInstance(getActivity())
						.UpdateNote(getPosition(), note.getText().toString());
		EventBus.getDefault().unregister(this);
		super.onPause();
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.note, menu);
		//getActionProvider() gets the ShareActionProvider out of the menu item.
		share = (ShareActionProvider)menu.findItem(R.id.share).getActionProvider();
		share.setShareIntent(shareIntent);
		super.onCreateOptionsMenu(menu, inflater);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if(item.getItemId() == R.id.delete){
			note.setText(null);
			getContract().closeNotes();
			return(true);
		}
			
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * here we confirm the event is for our fragment
	 * if the position matches.
	 * we populate the editext with the content.
	 */
	public void onEventMainThread(NoteLoadEvent event){
		if(event.getPosition() == getPosition()){
			note.setText(event.getProse());
		}
	}
	
	private contract getContract(){
		return((contract)getActivity());
	}

	private int getPosition(){
		return(getArguments().getInt(KEY_POSITION,-1));
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// I do not care about this 
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// I do not care about this
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		// Intent.EXTRA_TEXT as per andorid documentation for making ACTION_SEND work
		shareIntent.putExtra(Intent.EXTRA_TEXT, s.toString());
	}


	
	
	
}
