package com.override.ereader;

import java.util.Calendar;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class UpdateReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(intent.getAction()!=null){
			scheduleAlarm(context);
		}else{
			//third party library method. to handle the case where we get control due to the alarm event
			WakefulIntentService.sendWakefulWork(context, DownloadCheckService.class);
		}

	}
	
	/*
	 * method to schedule the alarms with AlarmManager.
	 */
	static void scheduleAlarm(Context ctxt){
		AlarmManager mngr = 
				(AlarmManager)ctxt.getSystemService(Context.ALARM_SERVICE);
		//creating an pending intent pointing back to the Update receiver
		Intent alarm = new Intent(ctxt,UpdateReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(ctxt, 0, alarm, 0);
		Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.HOUR_OF_DAY, 4);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		if(cal.getTimeInMillis() < System.currentTimeMillis()){
			cal.add(Calendar.DAY_OF_YEAR, 1);
		}
		
		mngr.setRepeating(AlarmManager.RTC_WAKEUP,
				cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
	}

}
