package com.override.ereader;

import de.greenrobot.event.EventBus;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Process;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "ereader.db";
	private static final int DATABASE_VERSION = 1;
	private static DatabaseHelper singleton = null;
	
	//static singleton instance and getInstance() to lazy-intialize it.
	synchronized static DatabaseHelper getInstance(Context ctxt){
		if(singleton == null){
			singleton = new DatabaseHelper(ctxt.getApplicationContext());
		}
		return(singleton);
	}
	
	private DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	    db.execSQL("CREATE TABLE notes (position INTEGER PRIMARY KEY, prose TEXT);");
		//position column indicating the chapter
		//prose column indicates the notes.
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	
	void loadNote(int position){
		new LoadThread(position).start();
	}
	
	void UpdateNote(int position, String prose){
		new UpdateThread(position, prose).start();
	}
	
	private class LoadThread extends Thread{
		private int position = -1;

		LoadThread(int position) {
			super();
			this.position = position;
			Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
		}
		/*
		 * (non-Javadoc)
		 * @see java.lang.Thread#run()
		 * Here we are doing a rawQuery to retrive the note based on the position.
		 * If there is no note, our cursor will have no rows and we are done.
		 * If we get a results back on the querym then post a NoteLoadedEvent with position and text.
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			String[] arg = {String.valueOf(position)};
			Cursor c = getReadableDatabase().rawQuery("SELECT prose FROM notes WHERE position = ?", arg);
			if(c.getCount() > 0){
				c.moveToFirst();
				EventBus.getDefault().post(new NoteLoadEvent(position, c.getString(0)));
			}
			c.close();
		}	
	}
	
	private class UpdateThread extends Thread{
		private int position = -1;
		private String prose = null;
		UpdateThread(int position, String prose) {
			super();
			this.position = position;
			this.prose = prose;
			Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.lang.Thread#run()
		 * 
		 * here we execute SQL o insert or replace sql statement.
		 * We insert a new row if there is no match on our primary key
		 * or it will update the other column if there is a match
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			String[] arg = {String.valueOf(position),prose};
			getWritableDatabase().execSQL("INSERT OR REPLACE INTO notes (position, prose) VALUES (?,?)",arg); 
		}
		
		
	}

}
