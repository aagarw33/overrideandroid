package com.override.ereader;

/*
 * EventBus class
 */
public class BookLoadEvent {
		
	private BookContents content = null;

	public BookLoadEvent(BookContents content) {
		this.content = content;
	}
	
	public BookContents getBook(){
		return(content);
	}
	
}
