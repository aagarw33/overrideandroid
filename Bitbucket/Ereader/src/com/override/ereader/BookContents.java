package com.override.ereader;

import java.io.File;
import java.util.List;

import android.net.Uri;

public class BookContents {

	String title;
	List<BookContents.Chapter> chapters;
	File baseDir = null;

	int getChapterCount() {
		return (chapters.size());
	}

	String getChapterFile(int position) {
		return (chapters.get(position).file);
	}

	String getTitle() {
		return (title);
	}

	/*
	 * this method uses getChapterFile() for getting the relative path from the
	 * book's JSON, then uses baseDir or android assest path to come up with
	 * full webview friendly path to the file
	 */
	String getChapterPath(int position) {
		String file = getChapterFile(position);
		if (baseDir == null) {
			return ("file:///android_asset/book/" + file);
		}
		return (Uri.fromFile(new File(baseDir, file)).toString());
	}

	void setBaseDir(File baseDir) {
		this.baseDir = baseDir;
	}

	static class Chapter {

		String file;
		String title;
	}
}
