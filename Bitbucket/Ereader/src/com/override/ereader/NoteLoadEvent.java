package com.override.ereader;


/*
 * as we are loading the notes from databases on background thread.
 * We position an event on the greenrobot eventbus when load is completed
 * that will deliver the result to NotesFragment
 */
public class NoteLoadEvent {

	int position;
	String prose;
	
	
	NoteLoadEvent(int position, String prose) {
		this.position = position;
		this.prose = prose;
	}
	
	int getPosition(){
		return(position);
	}
	
	String getProse(){
		return(prose);
	}
}
