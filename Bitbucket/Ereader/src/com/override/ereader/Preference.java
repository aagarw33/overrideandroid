package com.override.ereader;

import java.util.List;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

public class Preference extends PreferenceActivity {

	@Override
	public void onBuildHeaders(List<Header> target) {
		// TODO Auto-generated method stub
		loadHeadersFromResource(R.xml.preference_header, target);
	}

	@Override
	protected boolean isValidFragment(String fragmentName) {
		// TODO Auto-generated method stub
		if(Display.class.getName().equals(fragmentName)){
			return(true);
		}
		return(false);
	}

	public static class Display extends PreferenceFragment{

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_display);
		}
		
	}
}
