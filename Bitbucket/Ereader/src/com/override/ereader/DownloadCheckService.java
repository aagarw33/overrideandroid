package com.override.ereader;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import retrofit.RestAdapter;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

import de.greenrobot.event.EventBus;

public class DownloadCheckService extends WakefulIntentService {

	private static final String OUR_BOOK_DATE = "20120418";
	public static final String UPDATE_FILENAME = "book.zip";
	public static final String UPDATE_BASEDIR = "updates";
	private static final int NOTIFY_ID = 1337;// UID for notifications

	public DownloadCheckService() {
		super("DownloadCheckService");
	}

	@Override
	protected void doWakefulWork(Intent intent) {
		String url = getUpdateUrl();

		if (url != null) {
			try {
				File book = download(url);
				File updateDir = new File(getFilesDir(), UPDATE_BASEDIR);

				updateDir.mkdirs();
				unzip(book, updateDir);
				book.delete();

				EventBus.getDefault().register(this, 0);
				EventBus.getDefault().post(new BookUpdateEvent());
				EventBus.getDefault().unregister(this);
			} catch (Exception e) {
				Log.e(getClass().getSimpleName(),
						"Exception downloading update", e);
			}
		}
	}

	public void onEvent(BookUpdateEvent event) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		Intent toLaunch = new Intent(this, MainActivity.class);
		PendingIntent pi = PendingIntent.getActivity(this, 0, toLaunch, 0);

		builder.setAutoCancel(true)
				.setContentIntent(pi)
				.setContentTitle(getString(R.string.update_complete))
				.setContentText(getString(R.string.update_desc))
				.setSmallIcon(android.R.drawable.stat_sys_download_done)
				.setTicker(getString(R.string.update_complete));

		NotificationManager mgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mgr.notify(NOTIFY_ID, builder.build());
	}

	// method to download the zip file
	private File download(String url) throws MalformedURLException, IOException {
		File output = new File(getFilesDir(), UPDATE_FILENAME);
		if (output.exists()) {
			output.delete();
		}
		/*
		 * this is a typical HttpUrlConnection download logic, downloading the
		 * contents of the URl into FILe in our internal storage.
		 */
		HttpURLConnection con = (HttpURLConnection) new URL(url)
				.openConnection();

		FileOutputStream fos = new FileOutputStream(output.getPath());
		BufferedOutputStream out = new BufferedOutputStream(fos);
		try {
			InputStream in = con.getInputStream();
			byte[] buffer = new byte[17000];
			int len = 0;

			while ((len = in.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			out.flush();
		} finally {
			fos.getFD().sync();
			out.close();
			con.disconnect();
		}
		return (output);

	}

	private String getUpdateUrl() {
		/*
		 * Retrofit RestAdapter pointing to the server that is our "Web Service"
		 */
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(
				"http://commonsware.com").build();
		/*
		 * creating an instance of bookupdateinterface using the retrofit rest
		 * adapter.
		 */
		BookUpdateInterface updateInterface = restAdapter
				.create(BookUpdateInterface.class);
		// calling update on the bookupdate
		BookUpdateInfo info = updateInterface.update();

		// self explainatory conditional statement.
		if (info.updatedOn.compareTo(OUR_BOOK_DATE) > 0) {
			return (info.updateUrl);
		}

		return (null);
	}

	/*
	 * this how files are unzipped in Jave
	 */
	private static void unzip(File src, File dest) throws IOException {
		InputStream is = new FileInputStream(src);
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
		ZipEntry ze;

		dest.mkdirs();

		while ((ze = zis.getNextEntry()) != null) {
			byte[] buffer = new byte[16384];
			int count;
			FileOutputStream fos = new FileOutputStream(new File(dest,
					ze.getName()));
			BufferedOutputStream bos = new BufferedOutputStream(fos);

			try {
				while ((count = zis.read(buffer)) != -1) {
					bos.write(buffer, 0, count);
				}
				bos.flush();
			} finally {
				fos.getFD().sync();
				bos.close();
			}
			zis.closeEntry();
		}
		zis.close();
	}
}
