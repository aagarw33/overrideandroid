package com.override.ereader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Process;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

public class BookModelFragment extends Fragment {

	private SharedPreferences _prefs = null;// this will handle the
											// PreferenceReadyEvent, to load the
											// book in background thread
	private BookContents contents = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		/*
		 * Retain the fragment despite the configuration changes.
		 */
		setRetainInstance(true);
	}

	@Override
	public void onAttach(Activity host) {
		// TODO Auto-generated method stub
		super.onAttach(host);
		//'1' interested in receving events at high priority
		EventBus.getDefault().register(this,1);
		/*
		 * As we do not have book content. Starting the LoadThread thread to
		 * load the contents
		 */
		if (contents == null) {
			new LoadThread(host).start();
		}
	}

	@Override
	public void onDetach() {
		EventBus.getDefault().unregister(this);
		super.onDetach();
	}

	/*
	 * event method to respond to the bookupdate events
	 */
	public void onEvent(BookUpdateEvent event){
		if(getActivity()!=null){
			new LoadThread(getActivity()).start();
			EventBus.getDefault().cancelEventDelivery(event);
		}
	}
	public BookContents getBook() {
		return (contents);
	}

	public SharedPreferences getPrefs() {
		return (_prefs);

	}

	/*
	 * LoadThread takes the Context as parameter Set Thread Priority Background
	 * will reduce the CPU time.
	 */
	private class LoadThread extends Thread {
		private Context ctxt = null;

		LoadThread(Context _ctxt) {
			super();
			this.ctxt = _ctxt.getApplicationContext();// Saving the application
														// context
			Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run() Reading the JSON data using GSON to
		 * create an instance of BookContents class.
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			_prefs = PreferenceManager.getDefaultSharedPreferences(ctxt); // retriving
																			// the
																			// sharedpreference
			Gson _gson = new Gson();

			File baseDir = new File(ctxt.getFilesDir(),
					DownloadCheckService.UPDATE_BASEDIR);

			try {
				InputStream _is;
				/*
				 * checking if base directory exists or not if it does contents
				 * from its contents.json are used else we use from assets
				 * folder
				 */
				if (baseDir.exists()) {
					_is = new FileInputStream(
							new File(baseDir, "contents.json"));
				} else {
					_is = ctxt.getAssets().open("book/contents.json");// calling
																		// assets
																		// on
																		// context
				}
				BufferedReader _reader = new BufferedReader(
						new InputStreamReader(_is));

				contents = _gson.fromJson(_reader, BookContents.class);// will
																		// be
																		// loading
																		// an
																		// instance
																		// of
																		// BookContents
																		// object
				_is.close();
				if (baseDir.exists()) {
					contents.setBaseDir(baseDir);
				}
				EventBus.getDefault().post(new BookLoadEvent(contents));// posting
																		// the
																		// Event
																		// to
																		// BookLoadEvent
																		// EventBus.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	}

}
