package com.override.ereader;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

public class ContentFragmentAdapter extends FragmentStatePagerAdapter {

	private BookContents contents = null;
	public ContentFragmentAdapter(Activity fm,BookContents contents) {
		super(fm.getFragmentManager());
		// TODO Auto-generated constructor stub
		this.contents = contents;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.support.v13.app.FragmentStatePagerAdapter#getItem(int)
	 * will retrive the path for the given chapter
	 * from BookContent and creates a fragment
	 */

	@Override
	public Fragment getItem(int position) {
		return(ContentFragment.newInstance(contents.getChapterPath(position)));
	}

	/*
	 * (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 * returns the count of chapters from 
	 * BookContents.class
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return(contents.getChapterCount());
	}

}
