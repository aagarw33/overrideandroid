package com.override.ereader;

import org.w3c.dom.Notation;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

public class NotesActivity extends Activity implements NotesFragment.contract{

	public static final String EXTRA_POSITION = "position";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		if
		(getFragmentManager().findFragmentById(android.R.id.content) == null){
			int position = getIntent().getIntExtra(EXTRA_POSITION, -1);
			
			if(position >= 0 ){
				Fragment fr = NotesFragment.newInstance(position);
				getFragmentManager().beginTransaction()
							.add(android.R.id.content,fr).commit();
			}
					
		}
	}

	@Override
	public void closeNotes() {
		// TODO Auto-generated method stub
		finish();
	}
	
	
}
