package com.override.ereader;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebViewFragment;

public class ContentFragment extends WebViewFragment {
	
	private static final String TAG = "ContentFragment";
	private static final String KEY_FILE = "file";
	
	/*
	 * creates an instance of contentfragment
	 * puts it into the bundle(KEY_FILE)
	 * returns newly created fragment
	 */
	static ContentFragment newInstance(String file){
		ContentFragment _cf = new ContentFragment();
		
		Bundle args = new Bundle();
		args.putString(KEY_FILE, file);
		_cf.setArguments(args);
		return(_cf);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View _res = super.onCreateView(inflater, container, savedInstanceState);
		getWebView().getSettings().setJavaScriptEnabled(true); //enables javascript for webview
		getWebView().getSettings().setSupportZoom(true);		// now supports zoom
		getWebView().getSettings().setBuiltInZoomControls(true); //enabling built in zoom controls
		getWebView().loadUrl(getPage());
		return(_res);
	}

	private String getPage() {
		// TODO Auto-generated method stub	
		return(getArguments().getString(KEY_FILE));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	
}
