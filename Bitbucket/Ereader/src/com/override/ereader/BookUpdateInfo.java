package com.override.ereader;

/*
 * Json look like java class,
 * to check for the updated content
 * of the book
 */
public class BookUpdateInfo {
	
	String updatedOn;
	String updateUrl;
}
