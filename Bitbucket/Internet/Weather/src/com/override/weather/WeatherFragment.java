package com.override.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class WeatherFragment extends WebVuewFragment implements LocationListener{
	
	private String template = null;
	private LocationManager mngr = null;
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setRetainInstance(true);
		
		template = getActivity().getString(R.string.url);
		mngr =
				(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
	}

	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mngr.removeUpdates(this);
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mngr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				3600000, 1000, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		ForecastTask task = new ForecastTask();
		task.execute(location);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	private String getForecastXML(String path) throws IOException{
		BufferedReader reader = null;
		URL url = new URL(path);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		
		try{
			reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			StringBuilder sb  = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null){
				sb.append(line + "\n");
			}
			return(sb.toString());
		}finally{
			if (reader != null){
				reader.close();
			}
			con.disconnect();
		}
		
	}
	
	
	private ArrayList<Forecast> buildForecasts(String raw) throws Exception{
		
		ArrayList<Forecast> forecast = new ArrayList<Forecast>();
		DocumentBuilder build = 
				DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = build.parse(new InputSource(new StringReader(raw)));
		NodeList times = doc.getElementsByTagName("start-valid-time");
		
		for(int i=0;i<times.getLength();i++){
			Element time = (Element)times.item(i);
			Forecast fore = new Forecast();
			
			forecast.add(fore);
			fore.setTime(time.getFirstChild().getNodeValue());
		}
		
		NodeList temps = doc.getElementsByTagName("value");
		
		for(int i =0 ; i<temps.getLength();i++){
			Element temp =(Element)temps.item(i);
			Forecast fore = forecast.get(i);
			
			fore.setTemp(Integer.valueOf(temp.getFirstChild().getNodeValue()));
		}
		
		NodeList icon = doc.getElementsByTagName("icon-link");
		
		for(int i =0;i<icon.getLength();i++){
			Element icon_ = (Element)icon.item(i);
			Forecast fore = forecast.get(i);
			
			fore.setIconUrl(icon_.getFirstChild().getNodeValue());
		}
		return(forecast);
	}
	
	String generatePage(ArrayList<Forecast> forecast){
		StringBuilder bufResult=new StringBuilder("<html><body><table>");

	    bufResult.append("<tr><th width=\"50%\">Time</th>"
	        + "<th>Temperature</th><th>Forecast</th></tr>");

	    for (Forecast cast : forecast) {
	      bufResult.append("<tr><td align=\"center\">");
	      bufResult.append(cast.getTime());
	      bufResult.append("</td><td align=\"center\">");
	      bufResult.append(cast.getTemp());
	      bufResult.append("</td><td><img src=\"");
	      bufResult.append(cast.getIconUrl());
	      bufResult.append("\"></td></tr>");
	    }

	    bufResult.append("</table></body></html>");

	    return(bufResult.toString());
	}
	
	private class ForecastTask extends AsyncTask<Location, Void, String>{
		Exception e = null;

		@Override
		protected String doInBackground(Location... locs) {
			// TODO Auto-generated method stub
			String page = null;
			try{
				Location loc = locs[0];
				String url = 
							String.format(template, loc.getLatitude(),
									loc.getLongitude());
				page = generatePage(buildForecasts(getForecastXML(url)));
			}catch(Exception e){
				this.e = e;
			}
			return(page);
		}
		
	    @Override
	    protected void onPostExecute(String page) {
	    	
	    	if( e == null){
	    		getWebView().loadDataWithBaseURL(null, page, "text/html",
	    				"UTF-8", null);
	    	}else{
	    		Toast.makeText(getActivity(), String.format(getString(R.string.error),
	    				e.toString()), Toast.LENGTH_LONG).show();
	    	}
	    }
		
	}

}
