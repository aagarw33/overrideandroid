package com.override.weather;

class Forecast {

	String time = "";
	Integer temp = null;
	String iconUrl = "";

	String getTime() {
		return(time);
	}

	void setTime(String time) {
		this.time = time.substring(0, 16).replace('T', ' ');
	}

	Integer getTemp() {
		return(temp);
	}

	void setTemp(Integer temp) {
		this.temp = temp;
	}

	String getIconUrl() {
		return(iconUrl);
	}

	void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

}
