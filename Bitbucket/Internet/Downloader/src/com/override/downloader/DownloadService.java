package com.override.downloader;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class DownloadService extends IntentService {
	
	public static final String ACTION_COMPLETE
		="com.override.downloader.action.COMPLETE";
	
	public DownloadService(){
		super("DownloadService");
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}


	/*IntentService will stop itself automatically when there is no other request for downloads while onHandleIntent() is running
	
	 * onHandleIntent() is called on background thread.
	 * In this, we first set up a File object pointing to the location where we want to download the file. 
	 * If the directory does not exists on the location we create the directory using mkdirs().
	 * getLastPathSegment() method on Uri will return filename portion of path-style Uri.
	 * Then creating a typical HttpUrlConnection process to connect to the URL supplied via intent.
	 * 	 Finally sending the broadcast.
	 * */
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		try{
			File root = 
					Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			root.mkdirs();
			
			File output = new File(root,intent.getData().getLastPathSegment());
			
			if(output.exists()){
				output.delete();
			}
			
			URL url = new URL(intent.getData().toString());
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			
			FileOutputStream fos = new FileOutputStream(output.getPath());
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			
			try{
				InputStream in = con.getInputStream();
				byte[] buffer = new byte[8192];
				int len = 0;
				
				while((len = in.read(buffer)) >= 0){
					bos.write(buffer,0,len);
				}
				bos.flush();
				}finally{
					fos.getFD().sync();
					bos.close();
					con.disconnect();
			}
			LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_COMPLETE));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	

}
