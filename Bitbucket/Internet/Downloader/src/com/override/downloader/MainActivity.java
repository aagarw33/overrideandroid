package com.override.downloader;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	Button b;

	private BroadcastReceiver onEvent = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			b.setEnabled(true);

			Toast.makeText(getApplicationContext(), "Download Complete",
					Toast.LENGTH_SHORT).show();

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.download_test);

		b = (Button) findViewById(R.id.download);

		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				b.setEnabled(false);
				Intent i = new Intent(MainActivity.this, DownloadService.class);
				i.setData(Uri
						.parse("http://commonsware.com/Android/excerpt.pdf"));
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		IntentFilter filter = new IntentFilter(DownloadService.ACTION_COMPLETE);
		LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(
				onEvent, filter);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		LocalBroadcastManager.getInstance(MainActivity.this)
				.unregisterReceiver(onEvent);
	}

}
