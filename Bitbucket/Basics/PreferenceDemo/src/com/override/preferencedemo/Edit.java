package com.override.preferencedemo;

import java.util.List;

import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

public class Edit extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
			addPreferencesFromResource(R.xml.pref1);
			addPreferencesFromResource(R.xml.pref2);
		}
	}

	@Override
	public void onBuildHeaders(List<Header> target) {
		// TODO Auto-generated method stub
		loadHeadersFromResource(R.xml.preference_header, target);
		
	}

	@Override
	protected boolean isValidFragment(String fragmentName) {
		// TODO Auto-generated method stub
		if(First.class.getName().equals(fragmentName)
				||Second.class.getName().equals(fragmentName)){
			return(true);
		}
		return(false);
		
	}
	
	public static class First extends PreferenceFragment{

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref1);
		}
		
	}
	public static class Second extends PreferenceFragment{

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref2);
		}
		
	}
}
