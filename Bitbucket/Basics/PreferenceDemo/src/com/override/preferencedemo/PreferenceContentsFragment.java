package com.override.preferencedemo;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PreferenceContentsFragment extends Fragment {
	private TextView _checkbox = null;
	private TextView _checkbox2 = null;
	private TextView _ringtone = null;
	private TextView _text = null;
	private TextView _list = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View _res = inflater.inflate(R.layout.content, container, false);

		_checkbox = (TextView) _res.findViewById(R.id.checkbox);

		_checkbox2 = (TextView) _res.findViewById(R.id.checkbox2);

		_ringtone = (TextView) _res.findViewById(R.id.ringtone);

		_list = (TextView) _res.findViewById(R.id.list);

		_text = (TextView) _res.findViewById(R.id.text);
		return(_res);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		SharedPreferences _prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		
		_checkbox.setText(Boolean.valueOf(_prefs.getBoolean("checkbox", false)).toString());

		_checkbox2.setText(Boolean.valueOf(_prefs.getBoolean("checkbox2", false)).toString());
		
		_ringtone.setText(_prefs.getString("ringtone", "<unset>"));

		_list.setText(_prefs.getString("list", "<unset>"));

		_text.setText(_prefs.getString("text", "<unset>"));
	}

	
}
