package com.override.customizedlistview;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends ListActivity {
	private static final String TAG = "MainActivity";
	private static final String[] items = {"casillas","ramos","pepe","marcelo","daniel",
											"modric","dimaria","alonso","james","kroos",
											"ronaldo","bale","benzema"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new IconicAdapter());
	}
	
	class IconicAdapter extends ArrayAdapter<String>{

		public IconicAdapter() {
			super(MainActivity.this, R.layout.activity_main,R.id.label, items);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			View row =  super.getView(position, convertView, parent);
			ImageView icon = (ImageView)row.findViewById(R.id.icon);
			if(items[position].length()>4){
				icon.setImageResource(R.drawable.ok);
			}else{
				icon.setImageResource(R.drawable.wrong);
			}
			
			TextView size = (TextView)row.findViewById(R.id.size);
			size.setText("Size:"+items[position].length());
			return(row);
		}
		
		
		
	}
}
