package com.magician.viewpagerdemo;

import android.app.Activity;
import android.graphics.Color;
import android.media.JetPlayer.OnJetEventListener;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	ViewPager pager = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(new SampleAdapter());
		pager.setOffscreenPageLimit(6);
	}

	class SampleAdapter extends PagerAdapter {

		/*
		 * where you create the page itself and add it to the supplied
		 * container. We return some object that identifies this page, in this
		 * case we return the inflated View itself. A fragment-based
		 * PagerAdapter would probably return the fragment.
		 */
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			View page = getLayoutInflater().inflate(R.layout.page, container,
					false);
			TextView tv = (TextView) page.findViewById(R.id.text);
			int blue = position * 25;
			final String msg = String.format(getString(R.string.item),
					position + 1);
			tv.setText(msg);
			tv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG)
							.show();
				}
			});

			page.setBackgroundColor(Color.argb(255, 0, 0, blue));
			container.addView(page);
			return (page);
		}

		/*
		 * we need to clean up a page that is being removed from the pager,
		 * where the page is identified by the Object that we returned from
		 * instantiateItem().
		 */
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return (9);
		}

		/*
		 * or a given position how much horizontal space in the ViewPager should
		 * be given to this particular page. Each page has its own unique size,
		 * the return value is float.
		 */
		@Override
		public float getPageWidth(int position) {
			return (0.5f);
		}

		/*
		 * where we confirm whether some specific page in the pager is indeed
		 * tied to specific object returned from instantiateItem().
		 */
		@Override
		public boolean isViewFromObject(View view, Object object) {
			// TODO Auto-generated method stub
			return (view == object);
		}

	}
}
