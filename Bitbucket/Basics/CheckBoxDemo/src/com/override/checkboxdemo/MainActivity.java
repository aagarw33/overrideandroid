package com.override.checkboxdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;


public class MainActivity extends Activity implements OnCheckedChangeListener{
	  CheckBox _cb;
	  private static final String TAG = "MainActivity";
	  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        _cb = (CheckBox)findViewById(R.id.checkBox1);
        _cb.setOnCheckedChangeListener(this);
        
    }




	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		
		if(isChecked){
			_cb.setText(R.string.checked);
		}else{
			_cb.setText(R.string.unchecked);
		}
	}
}
