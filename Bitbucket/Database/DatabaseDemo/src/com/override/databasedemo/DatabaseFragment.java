package com.override.databasedemo;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class DatabaseFragment extends ListFragment implements DialogInterface.OnClickListener{

	private DatabaseHelper _dbhelp = null;
	static final String TAG = "DatabaseFragment";
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
		
		_dbhelp = new DatabaseHelper(getActivity());
		new LoadTask().execute();
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		((CursorAdapter)getListAdapter()).getCursor().close();
		_dbhelp.close();
	}




	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.actions, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
		case R.id.add:
			add();
			return(true);
		}
		return super.onOptionsItemSelected(item);
	}

	

	private void add() {
		// TODO Auto-generated method stub
		LayoutInflater in = getActivity().getLayoutInflater();
		View add = in.inflate(R.layout.add_edit, null);
		
		AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
		b.setTitle(R.string.add_title).setView(add)
			.setPositiveButton(R.string.ok, this)
			.setNegativeButton(R.string.cancel, null).show();
			
	}




	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		ContentValues cv =new ContentValues(2);
		AlertDialog ad = (AlertDialog)dialog;
		EditText title = (EditText)ad.findViewById(R.id.title);
		EditText value = (EditText)ad.findViewById(R.id.value);
		
		cv.put(DatabaseHelper.TITLE, title.getText().toString());
		cv.put(DatabaseHelper.VALUE, title.getText().toString());
		
		new InsertTask().execute(cv);
	}
	
	//Executing the query by calling rawQuery() which contains an SQL SELECT statement and an
	//optional string array of parameters to be used that can be null
	private Cursor Doquery(){
		String query = 
				String.format("SELECT _id, %s,%s FROM %s ORDER BY %s", 
						DatabaseHelper.TITLE,DatabaseHelper.VALUE,
						DatabaseHelper.TABLE,DatabaseHelper.TITLE);
		return (_dbhelp.getReadableDatabase().rawQuery(query, null));
	}
	
	
	private class LoadTask extends AsyncTask<Void, Void, Void>{
/*
 * Adding SimpleCursorAdapter that is similar to listadaptet but this is for Cursor(database)
 * Here we are telling the adapter to take the rows out of cursor and turning each into
 * inflater R.layout.row ViewGroup
 */
		private Cursor asyncCursor = null;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			asyncCursor = Doquery();
			asyncCursor.getCount(); //finding the number of rows in the result
			
			return(null);
		}
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			
			SimpleCursorAdapter sca;
			
			/*
			 * for each row the columns named title and value are to be poured into their
			 * respective text views.
			 */
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
				sca = 
						new SimpleCursorAdapter(getActivity(), R.layout.row,
								asyncCursor, new String[]{
							DatabaseHelper.TITLE,DatabaseHelper.VALUE
						}, new int[]{R.id.title,R.id.value},0);
			}else{
				sca = 
						new SimpleCursorAdapter(getActivity(), R.layout.row,
									asyncCursor, new String[]{
							DatabaseHelper.TITLE,DatabaseHelper.VALUE},
						 new int[]{R.id.title,R.id.value});
			}
			
			setListAdapter(sca);
		}	
		
	}
	
	/*
	 * this task handles user insertion task, we have to insert records in database 
	 * in a background task.
	 */
	private class InsertTask extends AsyncTask<ContentValues, Void, Void>{

		private Cursor insertCursor = null;
		
		@Override
		protected Void doInBackground(ContentValues... cv) {
			// TODO Auto-generated method stub
			_dbhelp.getWritableDatabase().insert(DatabaseHelper.TABLE,
					DatabaseHelper.TITLE, cv[0]);
			insertCursor = Doquery();// this is called again so as to retrive fresh copy of cursor, to perform adequate insertion.
			insertCursor.getCount();
			return(null);
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			((CursorAdapter)getListAdapter()).changeCursor(insertCursor);
		}
		
		
	}

}
