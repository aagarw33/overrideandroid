package com.override.databasedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.hardware.SensorManager;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "constants.db";
	private static final int DATABASE_VERSION = 1;
	static final String TITLE = "title";
	static final String VALUE = "value";
	static final String TABLE = "constants";
	static final String TAG = "DatabaseHelper";

	// constructor has activities context, Database name, a cursor factory
	// object that is optional and is typically null
	// database version
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub 
		
		db.execSQL("CREATE TABLE constants (_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, value REAL);");

		ContentValues __cv = new ContentValues();

		__cv.put(TITLE, "Gravity, Death Start 1");
		__cv.put(VALUE, SensorManager.GRAVITY_DEATH_STAR_I);
		db.insert(TABLE, TITLE, __cv);

		__cv.put(TITLE, "Gravity, Earth");
		__cv.put(VALUE, SensorManager.GRAVITY_EARTH);
		db.insert(TABLE, TITLE, __cv);

		__cv.put(TITLE, "Gravity, Juipter");
		__cv.put(VALUE, SensorManager.GRAVITY_JUPITER);
		db.insert(TABLE, TITLE, __cv);

		__cv.put(TITLE, "Gravity, MARS");
		__cv.put(VALUE, SensorManager.GRAVITY_MARS);
		db.insert(TABLE, TITLE, __cv);

		__cv.put(TITLE, "Gravity, Mercury");
		__cv.put(VALUE, SensorManager.GRAVITY_MERCURY);
		db.insert(TABLE, TITLE, __cv);

		__cv.put(TITLE, "Gravity, Moon");
		__cv.put(VALUE, SensorManager.GRAVITY_MOON);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Neptune");
		__cv.put(VALUE, SensorManager.GRAVITY_NEPTUNE);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Pluto");
		__cv.put(VALUE, SensorManager.GRAVITY_PLUTO);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Saturn");
		__cv.put(VALUE, SensorManager.GRAVITY_SATURN);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Sun");
		__cv.put(VALUE, SensorManager.GRAVITY_SUN);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, The Island");
		__cv.put(VALUE, SensorManager.GRAVITY_THE_ISLAND);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Uranus");
		__cv.put(VALUE, SensorManager.GRAVITY_URANUS);
		db.insert(TABLE, TITLE, __cv);
		
		__cv.put(TITLE, "Gravity, Venus");
		__cv.put(VALUE, SensorManager.GRAVITY_VENUS);
		db.insert(TABLE, TITLE, __cv);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
