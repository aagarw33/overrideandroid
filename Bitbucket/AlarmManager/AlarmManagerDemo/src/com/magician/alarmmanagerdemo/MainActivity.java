package com.magician.alarmmanagerdemo;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final int ALARM_ID = 1337;
	private static final int PERIOD = 5000;
	private PendingIntent pi = null;
	private AlarmManager alarm_mgr = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		alarm_mgr = (AlarmManager)getSystemService(ALARM_SERVICE);//obtaining an instance of ALarmManager
		pi = createPendingResult(ALARM_ID, new Intent(), 0);//creating pending intent by supplying empty Intent as out "result"
		/*
		 * calling setrepeating to alarmManager.
		 * the type of alarm we want, Elapsed_realtime indicates that we want to use a relative time base for when the
		 * first event should occur.
		 * the time when we want the first event to occur, in this case specified as a tome delta in milliseconds, added to "now"
		 * determined by Systemclock.elapsedRealtime()
		 * the number of milliseconds to occur between events.
		 * the pending intent to invoke for each of these events.
		 */
		alarm_mgr.setRepeating(AlarmManager.ELAPSED_REALTIME,
					SystemClock.elapsedRealtime() + PERIOD, PERIOD, pi);// calling set repeating to alarmmanager.
	}

	@Override
	protected void onDestroy() {
		alarm_mgr.cancel(pi);
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == ALARM_ID){
			Toast.makeText(this, "Hello Alarm", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
	
}
