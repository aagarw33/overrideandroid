package com.magician.scheduledalarm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class ScheduledService extends IntentService {

	public ScheduledService() {
		super("ScheduledService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(getClass().getSimpleName(), "I ran!");
	}

}
