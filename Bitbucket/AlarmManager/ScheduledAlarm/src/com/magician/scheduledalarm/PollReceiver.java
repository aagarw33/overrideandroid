package com.magician.scheduledalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

public class PollReceiver extends BroadcastReceiver {

	private static final int PERIOD = 5000;
	@Override
	public void onReceive(Context context, Intent intent) {
		scheduleAlarms(context);

	}
	
	/*
	 * this retrieves our alarmmanager, creates a pendingintent to call startservice.
	 */
	static void scheduleAlarms(Context ctxt){
		AlarmManager mngr = 
				(AlarmManager)ctxt.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(ctxt,ScheduledService.class);
		PendingIntent pi = PendingIntent.getService(ctxt, 0, i, 0);
		mngr.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime()+PERIOD,
					PERIOD, pi);
	}

}
