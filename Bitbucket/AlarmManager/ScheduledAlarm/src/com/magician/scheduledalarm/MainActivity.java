package com.magician.scheduledalarm;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		PollReceiver.scheduleAlarms(this);
		Toast.makeText(this, "Hello Alarm", Toast.LENGTH_LONG).show();
		finish();
	}
}
