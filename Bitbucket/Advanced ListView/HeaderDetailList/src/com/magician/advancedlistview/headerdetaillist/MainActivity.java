package com.magician.advancedlistview.headerdetaillist;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 
 * @author OverrideMAC
 * Demonstration of Advanced List View.
 * Here we are creating two separate Lists 
 * within the same layout.	
 * in this case we need to to have two key things:
	- creating rows for ourselves, using appropriate layout for required row type
	- recycle rows when they are provided, as this has major impact on scrolling speed of listview
 * getHeaderView() to simply the creation/recycling and populate the header rows.
 */
public class MainActivity extends ListActivity {

	private static final String[][] items = {
			{ "lorem", "ipsum", "dolor", "sit", "amet" },
			{ "consectetuer", "adipiscing", "elit", "morbi", "vel" },
			{ "ligula", "vitae", "arcu", "aliquet", "mollis" },
			{ "etiam", "vel", "erat", "placerat", "ante" },
			{ "porttitor", "sodales", "pellentesque", "augue", "purus" } };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(new IconicAdapter());
	}
	
	class IconicAdapter extends BaseAdapter{

		/*
		 * returns the total number of rows that would be in the list.
		 */
		@Override
		public int getCount() {
			int count = 0;
			
				for(String[] batch:items){
					count+=1+batch.length;
				}
			return(count);
		}

		/*
		 * needs to return the data model for a given position, 
		 * passed in as the typical int index.
		 */
		@Override
		public Object getItem(int position) {
			int offset = position;
			int batchIndex = 0;
			
				for (String[] batch : items){
					if(offset == 0){
						return(Integer.valueOf(batchIndex));
					}
					
					offset--;
					
					if(offset < batch.length){
							return(batch[offset]);
					}
					
					offset-=batch.length;
					batchIndex++;
				}
				throw new IllegalArgumentException("Invalid Position: "
						+String.valueOf(position));
		}

		//needs to return a unique long value for a given position.
		@Override
		public long getItemId(int position) {
			return(position);
		}

		//needs to return the number of distinct row types you will use.
		@Override
		public int getViewTypeCount() {
			return(2);
		}
		
		
		/*
		 * needs to return a value from 0 to getViewTypeCount() - 1, 
		 * indicating the index of the particular row type to use 
		 *  for a particular row position. 
		 */
		@Override
		public int getItemViewType(int position) {
				if(getItem(position) instanceof Integer){
					return(0);
				}
			return(1);
		}
		
		//which returns the view to use for a given row.
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(getItemViewType(position) == 0){
					return(getHeaderView(position,convertView,parent));
			}
			
			View row = convertView;
			
			if(row == null){
				row = getLayoutInflater().inflate(R.layout.row, parent,false);
			}
			
			ViewHolder holder = (ViewHolder)row.getTag();
			
			if(holder == null){
				holder = new ViewHolder(row);
				row.setTag(holder);
			}
			
			String word = (String)getItem(position);
			
			if(word.length() > 4){
					holder.icon.setImageResource(android.R.drawable.btn_star_big_off);
			}else{
					holder.icon.setImageResource(android.R.drawable.btn_star_big_on);
			}
			
			holder.label.setText(word);
			holder.size.setText(String.format(getString(R.string.size_template)
					, word.length()));
			return(row);
		}
		
		
		private View getHeaderView(int position,View convertView,ViewGroup parent){
			View row = convertView;
			
			if(row == null){
					row = getLayoutInflater().inflate(R.layout.header, parent,false);
			}
			
			Integer batchIndex = (Integer)getItem(position);
			TextView label = (TextView)row.findViewById(R.id.label);
			
			label.setText(String.format(getString(R.string.batch),
					1 + batchIndex.intValue()));
			return(row);
		}
	}
}
