/**
 * 
 */
package com.magician.advancedlistview.ratinglist;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * @author OverrideMAC
 * Demo for Custom Mutable Row List	
 */
public class RateListDemo extends ListActivity {
	private static final String[] items={"lorem", "ipsum", "dolor",
        "sit", "amet",
        "consectetuer", "adipiscing", "elit", "morbi", "vel",
        "ligula", "vitae", "arcu", "aliquet", "mollis",
        "etiam", "vel", "erat", "placerat", "ante",
        "porttitor", "sodales", "pellentesque", "augue", "purus"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ArrayList<RowModel> list = new ArrayList<RowModel>();
		
		for(String s : items){
			list.add(new RowModel(s));
		}
		
		setListAdapter(new RatingAdapter(list));
	}
	
	private RowModel getModel(int position){
		return(((RatingAdapter)getListAdapter()).getItem(position));
	}
	
	class RatingAdapter extends ArrayAdapter<RowModel>{
		RatingAdapter(ArrayList<RowModel> list){
			super(RateListDemo.this,R.layout.row,R.id.label,list);
		}

		/*
		 * lets the arrayadapter to inflate and recycle the row.
		 * the checks to see if we have a ViewHolder in row's tag if not create one.
		 * 
		 */
		public View getView(int position, View convertView,
					ViewGroup parent){
			View row = super.getView(position, convertView, parent);
			RatingBar bar = (RatingBar)row.getTag();
			
			if(bar == null){
				bar = (RatingBar)row.findViewById(R.id.rate);
				row.setTag(bar);
				
				/*
				 * listener that looks at row's tag and converts
				 * that into an Integer, representing the position
				 * within the ArrayAdapter that this row is displaying.
				 */
				RatingBar.OnRatingBarChangeListener l =
							new RatingBar.OnRatingBarChangeListener() {
								
								@Override
								public void onRatingChanged(RatingBar ratingBar, float rating,
										boolean fromUser) {
									Integer mPosition = (Integer)ratingBar.getTag();
									RowModel model = getModel(mPosition);
								
									model.rating = rating;
									
									LinearLayout parent = (LinearLayout)ratingBar.getParent();
									TextView label = (TextView)parent.findViewById(R.id.label);
									label.setText(model.toString());
								}
							};
						bar.setOnRatingBarChangeListener(l);
			}
			
			RowModel model = getModel(position);
			
			//pointing to position in adapter the row is displaying.
			bar.setTag(Integer.valueOf(position));
			bar.setRating(model.rating);
			return(row);
		}
	}
	
	class RowModel{
		String label;
		float rating = 2.0f;
		
		RowModel(String label){
			this.label = label;
		}
		
		public String toString(){
			if(rating >= 3.0){
				return(label.toUpperCase());
			}
			
		return(label);
		}
	}
}
