package com.magician.advancedlistview.headerfooterdemo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ListActivity {
	  private static String[] items={"lorem", "ipsum", "dolor",
          "sit", "amet", "consectetuer",
          "adipiscing", "elit", "morbi",
          "vel", "ligula", "vitae",
          "arcu", "aliquet", "mollis",
          "etiam", "vel", "erat",
          "placerat", "ante",
          "porttitor", "sodales",
          "pellentesque", "augue",
          "purus"};
	  
	  private long startTime = SystemClock.uptimeMillis();
	  private boolean areWeDeadYet = false;
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getListView().addHeaderView(buildHeader());
		getListView().addFooterView(buildFooter());
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1,
				items));		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		areWeDeadYet = true;
	}
	
	private View buildHeader(){
		Button btn = new Button(this);
		
		btn.setText("Randomize!");
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// returns the list of string in the array of items.
				List<String> list = Arrays.asList(items);
				
				Collections.shuffle(list);
				
				setListAdapter(new ArrayAdapter<String>(MainActivity.this, 
									android.R.layout.simple_list_item_1,
									list));
				
			}
		});
		
		return(btn);
	}

	private View buildFooter(){
		TextView txt = new TextView(this);
		updateFooter(txt);
		return(txt);
	}
	
	private void updateFooter(final TextView txt){
		long runtime = (SystemClock.uptimeMillis() - startTime)/1000;
		
		txt.setText(String.valueOf(runtime)+" seconds since activity launched");
		
		if(!areWeDeadYet){
			getListView().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					updateFooter(txt);
				}
			}, 1000);
		}
	}

}
