package com.magician.actionmode.actionmodeformultichoice;
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author OverrideMAC
 * 		   In this project, multi choice selection on a
 *         ListView row to bring up an action mode that replaces the context
 *         menu.
 */
public class MultiChoiceDemo extends ListActivity {

	private static final String[] items = { "lorem", "ipsum", "dolor", "sit",
			"amet", "consectetuer", "adipiscing", "elit", "morbi", "vel",
			"ligula", "vitae", "arcu", "aliquet", "mollis", "etiam", "vel",
			"erat", "placerat", "ante", "porttitor", "sodales", "pellentesque",
			"augue", "purus" };
	private ArrayList<String> words = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initAdapter();
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		getListView().setMultiChoiceModeListener(new MultiChoiceActionModeListener(this,
										getListView()));
		}else{
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		registerForContextMenu(getListView());
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			l.setItemChecked(position, true);
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.option, menu);

		EditText add = null;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			View v = menu.findItem(R.id.add).getActionView();

			if (v != null) {
				add = (EditText) v.findViewById(R.id.title);
			}
		}

		if (add != null) {
			add.setOnEditorActionListener(onSearch);
		}
		return (super.onCreateOptionsMenu(menu));
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			add();
			return (true);

		case R.id.reset:
			initAdapter();
			return (true);

		case R.id.about:
		case android.R.id.home:
			Toast.makeText(this, "Action Bar Sample App", Toast.LENGTH_LONG)
					.show();
			return (true);
		}

		return (super.onOptionsItemSelected(item));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		boolean res = performAction(item);
		
		if(!res){
			res = super.onContextItemSelected(item);
		}
		return(res);
	}
	/*
	 * handling date - change operations
	 */
	@SuppressWarnings("unchecked")
	public boolean performAction(MenuItem itemId) {
		ArrayAdapter<String> adapt = (ArrayAdapter<String>) getListAdapter();
		SparseBooleanArray checked = getListView().getCheckedItemPositions();
		switch (itemId.getItemId()) {
		case R.id.cap:
			for(int i=0;i < checked.size();i++){
			if(checked.valueAt(i)){
				int pos = checked.keyAt(i);
				String word = words.get(pos);
				word = word.toUpperCase(Locale.ENGLISH);
				adapt.remove(words.get(pos));
				adapt.insert(word, pos);
				}
			}
			return (true);
		case R.id.remove:
			ArrayList<Integer> positions = new ArrayList<Integer>();
			
			for(int i= 0;i<checked.size();i++){
				if(checked.valueAt(i)){
					positions.add(checked.keyAt(i));
				}
			}
			
			Collections.sort(positions,Collections.reverseOrder());
			for(int pos : positions){
			adapt.remove(words.get(pos));
			}
			
			getListView().clearChoices();
			return (true);
		}
		return (false);
	}

	private void initAdapter() {
		words = new ArrayList<String>();

		for (String s : items) {
			words.add(s);
		}

		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_checked, words));
	}

	@SuppressLint("InflateParams")
	private void add() {
		final View addView = getLayoutInflater().inflate(R.layout.add, null);

		new AlertDialog.Builder(this).setTitle("Add a Word").setView(addView)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						addWord((TextView) addView.findViewById(R.id.title));
					}
				}).setNegativeButton("Cancel", null).show();
	}

	@SuppressWarnings("unchecked")
	private void addWord(TextView title) {
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListAdapter();

		adapter.add(title.getText().toString());
	}

	private TextView.OnEditorActionListener onSearch = new TextView.OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
				addWord(v);
				InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				im.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			return (true);
		}
	};

}
