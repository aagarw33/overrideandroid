/**
 * 
 */
package com.magician.actionmode.actionmodeformultichoice;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

/**
 * @author OverrideMAC
 * AdapterView.OnItemLongClickListener as we want actionmode to
 * trigger on long press.
 */
public class MultiChoiceActionModeListener implements AbsListView.MultiChoiceModeListener {

	MultiChoiceDemo host;
	ActionMode activeMode;
	ListView modeView;

	public MultiChoiceActionModeListener(MultiChoiceDemo host, ListView modeView) {
		this.host = host;
		this.modeView = modeView;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.ActionMode.Callback#onCreateActionMode(android.view.ActionMode, android.view.Menu)
	 * 
	 * called shortly after you call startActionMode(), 
	 * it is just like onCreateOptionsMenu() except here you
	 * also have an ActionMode object. You can inflate a menu resource as well.
	 */
	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater mInflate = host.getMenuInflater();
		
		mInflate.inflate(R.menu.context, menu);
		mode.setTitle(R.string.context_title);
		mode.setSubtitle("(1)");
		activeMode = mode;
		return (true);
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.ActionMode.Callback#onPrepareActionMode(android.view.ActionMode, android.view.Menu)
	 * 
	 * triggered when you call invalidate() on Action Mode,
	 * You call invalidate() on Action Mode if you want to make changes to its contents
	 */
	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.ActionMode.Callback#onActionItemClicked(android.view.ActionMode, android.view.MenuItem)
	 * 
	 * this is similar to onOptionsItemSelected(), this is called if user clicked something related to the
	 * action mode.
	 */
	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		boolean res =
				host.performAction(item);
		
		updateSubtitle(activeMode);
		return(res);
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.ActionMode.Callback#onDestroyActionMode(android.view.ActionMode)
	 * 
	 * will be invoked when the action mode goes away.
	 */
	@Override
	public void onDestroyActionMode(ActionMode mode) {
		activeMode = null;

	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		updateSubtitle(mode);
	}
	
	private void updateSubtitle(ActionMode mode)
	{
		mode.setSubtitle("(" + modeView.getCheckedItemCount() + ")");
	}
}
