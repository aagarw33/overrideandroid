package com.magician.actionmode.manualnatiive;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author OverrideMAC In this project, we stick with the classic long-tap on a
 *         ListView row to bring up an action mode that replaces the context
 *         menu.
 */
public class SingleChoiceDemo extends ListActivity {

	private static final String[] items = { "lorem", "ipsum", "dolor", "sit",
			"amet", "consectetuer", "adipiscing", "elit", "morbi", "vel",
			"ligula", "vitae", "arcu", "aliquet", "mollis", "etiam", "vel",
			"erat", "placerat", "ante", "porttitor", "sodales", "pellentesque",
			"augue", "purus" };
	private ArrayList<String> words = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initAdapter();
		getListView().setLongClickable(true);
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		getListView().setOnItemLongClickListener(
				new ActionModeHelper(this, getListView()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.option, menu);

		EditText add = null;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			View v = menu.findItem(R.id.add).getActionView();

			if (v != null) {
				add = (EditText) v.findViewById(R.id.title);
			}
		}

		if (add != null) {
			add.setOnEditorActionListener(onSearch);
		}
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			add();
			return (true);

		case R.id.reset:
			initAdapter();
			return (true);

		case R.id.about:
		case android.R.id.home:
			Toast.makeText(this, "Action Bar Sample App", Toast.LENGTH_LONG)
					.show();
			return (true);
		}

		return (super.onOptionsItemSelected(item));
	}

	/*
	 * handling date - change operations
	 */
	@SuppressWarnings("unchecked")
	public boolean performAction(int itemId, int position) {
		ArrayAdapter<String> adapt = (ArrayAdapter<String>) getListAdapter();
		switch (itemId) {
		case R.id.cap:
			String word = words.get(position);
			word = word.toUpperCase();
			adapt.remove(words.get(position));
			adapt.insert(word, position);
			return (true);
		case R.id.remove:
			adapt.remove(words.get(position));
			return (true);
		}
		return (false);
	}

	private void initAdapter() {
		words = new ArrayList<String>();

		for (String s : items) {
			words.add(s);
		}

		setListAdapter(new ArrayAdapter<String>(this,
				R.layout.simple_list_item_1, words));
	}

	@SuppressLint("InflateParams")
	private void add() {
		final View addView = getLayoutInflater().inflate(R.layout.add, null);

		new AlertDialog.Builder(this).setTitle("Add a Word").setView(addView)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						addWord((TextView) addView.findViewById(R.id.title));
					}
				}).setNegativeButton("Cancel", null).show();
	}

	@SuppressWarnings("unchecked")
	private void addWord(TextView title) {
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListAdapter();

		adapter.add(title.getText().toString());
	}

	private TextView.OnEditorActionListener onSearch = new TextView.OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
				addWord(v);
				InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				im.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			return (true);
		}
	};

}
