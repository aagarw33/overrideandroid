package com.magician.actionmode.multilongclickactionmode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author OverrideMAC In this project, long click multi choice selection on a
 *         ListView row to bring up an action mode that replaces the context
 *         menu.
 */
public class LongClickMultiChoiceDemo extends ListActivity implements
		OnItemLongClickListener, AbsListView.MultiChoiceModeListener {

	private static final String[] items = { "lorem", "ipsum", "dolor", "sit",
			"amet", "consectetuer", "adipiscing", "elit", "morbi", "vel",
			"ligula", "vitae", "arcu", "aliquet", "mollis", "etiam", "vel",
			"erat", "placerat", "ante", "porttitor", "sodales", "pellentesque",
			"augue", "purus" };
	private ArrayList<String> words = null;
	private static final String STATE_CHOICE_MODE = "choiceMode";
	private static final String STATE_MODEL = "model";
	private ActionMode activeMode = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState == null) {
			initAdapter(null);
		} else {
			initAdapter(savedInstanceState.getStringArrayList(STATE_MODEL));
		}

		getListView().setOnItemLongClickListener(this);
		getListView().setMultiChoiceModeListener(this);

		int choiceMode = (savedInstanceState == null ? ListView.CHOICE_MODE_NONE
				: savedInstanceState.getInt(STATE_CHOICE_MODE));
		getListView().setChoiceMode(choiceMode);

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			l.setItemChecked(position, true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.option, menu);

		EditText add = null;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			View v = menu.findItem(R.id.add).getActionView();

			if (v != null) {
				add = (EditText) v.findViewById(R.id.title);
			}
		}

		if (add != null) {
			add.setOnEditorActionListener(onSearch);
		}
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			add();
			return (true);

		case R.id.reset:
			initAdapter(null);
			return (true);

		case R.id.about:
		case android.R.id.home:
			Toast.makeText(this, "Action Bar Sample App", Toast.LENGTH_LONG)
					.show();
			return (true);
		}

		return (super.onOptionsItemSelected(item));
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		boolean res = performAction(item);

		if (!res) {
			res = super.onContextItemSelected(item);
		}
		return (res);
	}

	/*
	 * handling date - change operations
	 */
	@SuppressWarnings("unchecked")
	public boolean performAction(MenuItem itemId) {
		ArrayAdapter<String> adapt = (ArrayAdapter<String>) getListAdapter();
		SparseBooleanArray checked = getListView().getCheckedItemPositions();
		switch (itemId.getItemId()) {
		case R.id.cap:
			for (int i = 0; i < checked.size(); i++) {
				if (checked.valueAt(i)) {
					int pos = checked.keyAt(i);
					String word = words.get(pos);
					word = word.toUpperCase(Locale.ENGLISH);
					adapt.remove(words.get(pos));
					adapt.insert(word, pos);
				}
			}
			return (true);
		case R.id.remove:
			ArrayList<Integer> positions = new ArrayList<Integer>();

			for (int i = 0; i < checked.size(); i++) {
				if (checked.valueAt(i)) {
					positions.add(checked.keyAt(i));
				}
			}

			Collections.sort(positions, Collections.reverseOrder());
			for (int pos : positions) {
				adapt.remove(words.get(pos));
			}

			getListView().clearChoices();
			return (true);
		}
		return (false);
	}

	private void initAdapter(ArrayList<String> startPoint) {
		if (startPoint == null) {
			words = new ArrayList<String>();

			for (String s : items) {
				words.add(s);
			}
		} else {
			words = startPoint;
		}
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_activated_1, words));
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_CHOICE_MODE, getListView().getChoiceMode());
		outState.putStringArrayList(STATE_MODEL, words);
	}

	@SuppressLint("InflateParams")
	private void add() {
		final View addView = getLayoutInflater().inflate(R.layout.add, null);

		new AlertDialog.Builder(this).setTitle("Add a Word").setView(addView)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						addWord((TextView) addView.findViewById(R.id.title));
					}
				}).setNegativeButton("Cancel", null).show();
	}

	@SuppressWarnings("unchecked")
	private void addWord(TextView title) {
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListAdapter();

		adapter.add(title.getText().toString());
	}

	private TextView.OnEditorActionListener onSearch = new TextView.OnEditorActionListener() {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event == null || event.getAction() == KeyEvent.ACTION_UP) {
				addWord(v);
				InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				im.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			return (true);
		}
	};

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater mInflate = getMenuInflater();

		mInflate.inflate(R.menu.context, menu);
		mode.setTitle(R.string.context_title);
		mode.setSubtitle("(1)");
		activeMode = mode;
		updateSubTitle(activeMode);
		return (true);
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return (false);
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		boolean res = performAction(item);
		updateSubTitle(activeMode);
		return (res);
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		if (activeMode != null) {
			activeMode = null;
			getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
			getListView().setAdapter(getListView().getAdapter());
		}
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode mode, int position,
			long id, boolean checked) {
		if (activeMode != null) {
			updateSubTitle(mode);
		}
	}

	private void updateSubTitle(ActionMode mode) {
		mode.setSubtitle("(" + getListView().getCheckedItemCount() + ")");
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		getListView().setItemChecked(position, true);
		return false;
	}

}
