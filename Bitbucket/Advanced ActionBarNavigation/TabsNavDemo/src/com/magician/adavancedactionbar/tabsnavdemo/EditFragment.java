
package com.magician.adavancedactionbar.tabsnavdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * @author OverrideMAC
 * 
 *         Utilizing the 'caveman' approach of replacing our entire fragment on
 *         each tab click.
 */
public class EditFragment extends Fragment {
	private EditText editor = null;
	private static final String KEY_POSITION = "position";
	private static final String KEY_TEXT = "text";

	static EditFragment newInstance(int position, CharSequence text) {
		EditFragment frag = new EditFragment();
		Bundle args = new Bundle();
		args.putInt(KEY_POSITION, position);
		args.putCharSequence(KEY_TEXT, text);
		frag.setArguments(args);
		return (frag);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View res = inflater.inflate(R.layout.editor, container, false);
		editor = (EditText) res.findViewById(R.id.editor);

		int position = getArguments().getInt(KEY_POSITION, -1);

		editor.setHint(String.format(getString(R.string.hint), position + 1));
		editor.setText(getArguments().getCharSequence(KEY_TEXT));
		return (res);
	}

	CharSequence getText() {
		return (editor.getText());
	}

}
