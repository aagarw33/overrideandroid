package com.magician.adavancedactionbar.tabsnavdemo;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ArrayAdapter;

/**
 * 
 * @author OverrideMAC We want to display full-screen EditText whose contents
 *         will be driven by tab navigation selection.
 * 
 * The implementation is similar to List Navigation expect we need to perform the following
 * actions of the fragment:
 * 		-	Add or remove fragments
 * 		-	Attach and detach fragments 
 * 		-	Flip pages of a Viewpager
 * 		-	Update a simple UI in a place.
 */

public class MainActivity extends FragmentActivity implements
		TabListener {

	private static final String KEY_MODELS = "models";
	private static final String KEY_POSITION = "position";
	private CharSequence[] models = new CharSequence[10]; // to store our data model
	private ArrayAdapter<String> nav = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		// restoring our models of array
		if (savedInstanceState != null) {
			models = savedInstanceState.getCharSequenceArray(KEY_MODELS);
		}

		ActionBar bar = getActionBar();
		
		// indicating what kind of navigation mode we want to set
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for(int i = 0; i < 10; i++){
			bar.addTab(bar.newTab().setText("Tab #"+ String.valueOf( i + 1))
					.setTabListener(this).setTag(i));
		}
		
		// telling the action bar which position to select.
		if (savedInstanceState != null) {
			bar.setSelectedNavigationItem(savedInstanceState
					.getInt(KEY_POSITION));
		}
	}

	
	//tracking last known position of Spinner selection and storing our data model in CharSequence array.
	@Override
	protected void onSaveInstanceState(Bundle outState) {

		outState.putCharSequenceArray(KEY_MODELS, models);
		outState.putInt(KEY_POSITION, getActionBar()
				.getSelectedNavigationIndex());
	}

	// this will get called when the Tab selected by the user.
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		int i = ((Integer)tab.getTag()).intValue();
		
		ft.replace(android.R.id.content,
						EditFragment.newInstance(i, models[i]));
	}

	// this will get called when some other tab is selected by the user
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		int i = ((Integer)tab.getTag()).intValue();
		EditFragment frag = 
					(EditFragment)getFragmentManager().findFragmentById(android.R.id.content);
		
		if(frag!=null){
			models[i] = frag.getText();
		}
	}

	// is called when the user taps on an already-selected tab.
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

}
