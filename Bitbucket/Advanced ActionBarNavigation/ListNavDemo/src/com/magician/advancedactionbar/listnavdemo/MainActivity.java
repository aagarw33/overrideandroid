package com.magician.advancedactionbar.listnavdemo;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ArrayAdapter;

/**
 * 
 * @author OverrideMAC We want to display full-screen EditText whose contents
 *         will be driven by list navigation selection.
 * @param <T>
 */
public class MainActivity extends FragmentActivity implements
		OnNavigationListener {

	private static final String KEY_MODELS = "models";
	private static final String KEY_POSITION = "position";
	private static final String[] labels = { "Editor #1", "Editor #2",
			"Editor #3", "Editor #4", "Editor #5", "Editor #6", "Editor #7",
			"Editor #8", "Editor #9", "Editor #10" };
	private CharSequence[] models = new CharSequence[10]; // to store our data model
	private EditFragment frag = null;
	private int lastPosition = -1; // to track last known position of Spinner selection
	private ArrayAdapter<String> nav = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// setting up the edit fragment if it does not exist
		frag = (EditFragment) getFragmentManager().findFragmentById(
				android.R.id.content);

		if (frag == null) {
			frag = new EditFragment();
			getFragmentManager().beginTransaction()
					.add(android.R.id.content, frag).commit();
		}

		// restoring our models of array
		if (savedInstanceState != null) {
			models = savedInstanceState.getCharSequenceArray(KEY_MODELS);
		}

		ActionBar bar = getActionBar();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			nav = new ArrayAdapter<String>(bar.getThemedContext(),
					android.R.layout.simple_spinner_item, labels);
		} else {
			nav = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, labels);
		}

		nav.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		// indicating what kind of navigation mode we want to set
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		// to supply items to our array adapter.
		bar.setListNavigationCallbacks(nav, this);

		
		// telling the action bar which position to select.
		if (savedInstanceState != null) {
			bar.setSelectedNavigationItem(savedInstanceState
					.getInt(KEY_POSITION));
		}
	}

	
	//tracking last known position of Spinner selection and storing our data model in CharSequence array.
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (lastPosition > -1) {
			models[lastPosition] = frag.getText();
		}

		outState.putCharSequenceArray(KEY_MODELS, models);
		outState.putInt(KEY_POSITION, getActionBar()
				.getSelectedNavigationIndex());
	}

	// this will get called when the Spinner selection changes.
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if (lastPosition > -1) {
			models[lastPosition] = frag.getText();
		}

		lastPosition = itemPosition;
		frag.setText(models[itemPosition]);
		frag.setHint(labels[itemPosition]);
		return (true);
	}

}
