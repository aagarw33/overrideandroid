/**
 * 
 */
package com.magician.advancedactionbar.listnavdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * @author OverrideMAC
 *	
 */
public class EditFragment extends Fragment {
	private EditText editor = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View res = inflater.inflate(R.layout.editor, container,false);
		editor = (EditText)res.findViewById(R.id.editor);
		
		return(res);
	}
	
	CharSequence getText(){
		return(editor.getText());
	}
	
	void setText(CharSequence text){
		editor.setText(text);
	}
	
	void setHint(CharSequence hint){
		editor.setHint(hint);
	}
}
