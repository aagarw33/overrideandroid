package com.magician.screentacticsslidingpanedemo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * 
 * @author OverrideMAC
 *	This fragment is responsible for rendering the list of
 *	nations.
 *	It is a ListFragment and is using a custom ArrayAdapter to 
 *	populate the List.
 */
public class CountriesFragment extends ContractListFragment<CountriesFragment.Contract>{
	static private final String STATE_CHECKED =
			 "com.magician.screentacticsdemo.STATE_CHECKED";
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setListAdapter(new CountryAdapter());
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);//sets up the list view with single choice mode.
		
		if(savedInstanceState!=null){
			int position = savedInstanceState.getInt(STATE_CHECKED,-1);
			if(position > -1){
				getListView().setItemChecked(position, true);
				getContract().onCountrySelected(Country.cont.get(position));
			}
		}
	}
	
	/*
	 * this code set the list item to be activated stated when clicked upon 
	 * apart from getting the details of the row selected. This provides visual
	 * context to the user and is often used in list-and-details pattern.
	 */
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

			l.setItemChecked(position, true);
			getContract().onCountrySelected(Country.cont.get(position));
	}

	class CountryAdapter extends ArrayAdapter<Country>{
		CountryAdapter(){
			super(getActivity(),R.layout.row,R.id.name,Country.cont);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			CountryViewHolder wrapper = null;
			
			if(convertView == null){
				convertView = 
						LayoutInflater.from(getActivity()).inflate(R.layout.row, null);
				wrapper = new CountryViewHolder(convertView);
				convertView.setTag(wrapper);
				}else{
					wrapper = (CountryViewHolder)convertView.getTag();
				}
			wrapper.populateFrom(getItem(position));
			return (convertView);
		}
	}
	
	interface Contract{
		void onCountrySelected(Country c);
	}
}
