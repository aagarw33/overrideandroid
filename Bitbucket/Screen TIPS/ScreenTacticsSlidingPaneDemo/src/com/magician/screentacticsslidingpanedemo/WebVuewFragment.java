package com.magician.screentacticsslidingpanedemo;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class WebVuewFragment extends Fragment {

	private WebView mWeb;
	private boolean ismWebAvailabe;
	
	public WebVuewFragment() {
		super();
	}
	
	 /**
	   * Called to instantiate the view. Creates and returns the
	   * WebView.
	   */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(mWeb != null){
			mWeb.destroy();
		}
		
		mWeb = new WebView(getActivity());
		ismWebAvailabe = true;
		
		return mWeb;
	}
	
	  /**
	   * Called when the fragment is visible to the user and
	   * actively running. Resumes the WebView.
	   */
	  @TargetApi(11)
	  @Override
	  public void onPause() {
	    super.onPause();

	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	      mWeb.onPause();
	    }
	  }

	  /**
	   * Called when the fragment is no longer resumed. Pauses
	   * the WebView.
	   */
	  @TargetApi(11)
	  @Override
	  public void onResume() {
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	      mWeb.onResume();
	    }

	    super.onResume();
	  }

	  /**
	   * Called when the WebView has been detached from the
	   * fragment. The WebView is no longer available after this
	   * time.
	   */
	  @Override
	  public void onDestroyView() {
	    ismWebAvailabe=false;
	    super.onDestroyView();
	  }

	  /**
	   * Called when the fragment is no longer in use. Destroys
	   * the internal state of the WebView.
	   */
	  @Override
	  public void onDestroy() {
	    if (mWeb != null) {
	      mWeb.destroy();
	      mWeb=null;
	    }
	    super.onDestroy();
	  }

	  /**
	   * Gets the WebView.
	   */
	  public WebView getWebView() {
	    return ismWebAvailabe ? mWeb : null;
	  }
	
	
}
