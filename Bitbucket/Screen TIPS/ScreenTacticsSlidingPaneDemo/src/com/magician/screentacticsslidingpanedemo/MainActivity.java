package com.magician.screentacticsslidingpanedemo;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SlidingPaneLayout;

/**
 * 
 * @author OverrideMAC
 * The result of this activity is to handle both the 
 * situations, where we have both fragments or just one.
 */
public class MainActivity extends FragmentActivity implements
			CountriesFragment.Contract{

	private SlidingPaneLayout panes = null;
	private DetailFragment details = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		/*
		 * find the detail fragment
		 */
		details = 
				(DetailFragment)getFragmentManager().findFragmentById(R.id.details);
		panes = (SlidingPaneLayout)findViewById(R.id.panes);
		panes.openPane();
		
	}

	@Override
	public void onBackPressed() {
		if(panes.isOpen()){
			super.onBackPressed();
		}
		else{
			panes.openPane();
		}
		
	}
	
	@Override
	public void onCountrySelected(Country c) {
		details.loadUrl(getString(c.url));
		panes.closePane();
		
	}

}
