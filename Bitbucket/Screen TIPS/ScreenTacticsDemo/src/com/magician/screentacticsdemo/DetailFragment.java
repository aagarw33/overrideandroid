/**
 * 
 */
package com.magician.screentacticsdemo;

/**
 * @author OverrideMAC
 *	the details to be displayed come in the form of 
 *	URL designed to be displayed in the WebView.
 *	This fragment itself, needs to expose some methods to 
 *	allow a hosting activity to tell what URL to 
 *	display
 */
public class DetailFragment extends WebVuewFragment {
	public void loadUrl(String url){
		getWebView().loadUrl(url);
	}
}
