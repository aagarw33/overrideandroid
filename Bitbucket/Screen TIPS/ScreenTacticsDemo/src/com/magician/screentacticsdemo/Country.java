package com.magician.screentacticsdemo;

import java.util.ArrayList;

/**
 * 
 * @author OverrideMAC 
 * 		   This class holds the country name as string resourceID
 *         flag as resource Drawable and mobile wikipedia URL as another string
 *         resourceID
 */
public class Country {
	int name;
	int flag;
	int url;

	static ArrayList<Country> cont = new ArrayList<Country>();

	static {
		cont.add(new Country(R.string.austria, R.drawable.austria,
				R.string.austria_url));
		cont.add(new Country(R.string.belgium, R.drawable.belgium,
				R.string.belgium_url));
		cont.add(new Country(R.string.bulgaria, R.drawable.bulgaria,
				R.string.bulgaria_url));
		cont.add(new Country(R.string.cyprus, R.drawable.cyprus,
				R.string.cyprus_url));
		cont.add(new Country(R.string.czech_republic,
				R.drawable.czech_republic, R.string.czech_republic_url));
		cont.add(new Country(R.string.denmark, R.drawable.denmark,
				R.string.denmark_url));
		cont.add(new Country(R.string.estonia, R.drawable.estonia,
				R.string.estonia_url));
		cont.add(new Country(R.string.finland, R.drawable.finland,
				R.string.finland_url));
		cont.add(new Country(R.string.france, R.drawable.france,
				R.string.france_url));
		cont.add(new Country(R.string.germany, R.drawable.germany,
				R.string.germany_url));
		cont.add(new Country(R.string.greece, R.drawable.greece,
				R.string.greece_url));
		cont.add(new Country(R.string.hungary, R.drawable.hungary,
				R.string.hungary_url));
		cont.add(new Country(R.string.ireland, R.drawable.ireland,
				R.string.ireland_url));
		cont.add(new Country(R.string.italy, R.drawable.italy,
				R.string.italy_url));
		cont.add(new Country(R.string.latvia, R.drawable.latvia,
				R.string.latvia_url));
		cont.add(new Country(R.string.lithuania, R.drawable.lithuania,
				R.string.lithuania_url));
		cont.add(new Country(R.string.luxembourg, R.drawable.luxembourg,
				R.string.luxembourg_url));
		cont.add(new Country(R.string.malta, R.drawable.malta,
				R.string.malta_url));
		cont.add(new Country(R.string.netherlands, R.drawable.netherlands,
				R.string.netherlands_url));
		cont.add(new Country(R.string.poland, R.drawable.poland,
				R.string.poland_url));
		cont.add(new Country(R.string.portugal, R.drawable.portugal,
				R.string.portugal_url));
		cont.add(new Country(R.string.romania, R.drawable.romania,
				R.string.romania_url));
		cont.add(new Country(R.string.slovakia, R.drawable.slovakia,
				R.string.slovakia_url));
		cont.add(new Country(R.string.slovenia, R.drawable.slovenia,
				R.string.slovenia_url));
		cont.add(new Country(R.string.spain, R.drawable.spain,
				R.string.spain_url));
		cont.add(new Country(R.string.sweden, R.drawable.sweden,
				R.string.sweden_url));
		cont.add(new Country(R.string.united_kingdom,
				R.drawable.united_kingdom, R.string.united_kingdom_url));
	}

	public Country(int name, int flag, int url) {
		this.name = name;
		this.flag = flag;
		this.url = url;
	}

}
