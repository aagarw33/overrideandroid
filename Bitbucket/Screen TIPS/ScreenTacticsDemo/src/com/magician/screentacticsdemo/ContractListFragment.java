/**
 * 
 */
package com.magician.screentacticsdemo;

import android.app.Activity;
import android.app.ListFragment;

/**
 * @author OverrideMAC
 *	This is a ListFragment, that is based upon "contract" pattern.
 */
public class ContractListFragment<T> extends ListFragment {
	private T contract;
	
	@SuppressWarnings("unchecked")
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try{
			contract = (T)activity;
		}catch(ClassCastException e){
			throw new IllegalStateException(activity.getClass()
						.getSimpleName(),e);
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		contract = null;
	}
	
	public final T getContract(){
		return(contract);
	}
}
