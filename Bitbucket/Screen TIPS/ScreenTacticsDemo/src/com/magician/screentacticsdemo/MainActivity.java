package com.magician.screentacticsdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * 
 * @author OverrideMAC
 * The result of this activity is to handle both the 
 * situations, where we have both fragments or just one.
 */
public class MainActivity extends FragmentActivity implements
			CountriesFragment.Contract{

	private CountriesFragment countries = null;
	private DetailFragment details = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		/*
		 * if we already have instance of CountriesFragment, by asking fragment manager 
		 * to give us fragment R.id.countries slot - this mite occur if we went
		 * configuration changes.
		 */
		countries =
				(CountriesFragment)getFragmentManager().findFragmentById(R.id.countries);
		/*
		 * if we do not have CountriesFragment instance, create one and excute the 
		 * transaction to load R.id.countries.
		 */
		if(countries ==null){
			countries = new CountriesFragment();
			getFragmentManager().beginTransaction()
						.add(R.id.countries, countries)
						.commit();
		}
		
		/*
		 * find the detail fragment
		 */
		details = 
				(DetailFragment)getFragmentManager().findFragmentById(R.id.details);
		/*
		 * if we do not have detail fragment, and the layout has the R.id.detail fragment slot
		 * create a DetailFragment and execute the transaction.
		 */
		if(details == null && findViewById(R.id.details) !=null){
			details = new DetailFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.details, details).commit();
		}
	}

	/*
	 * check if we have details fragment or not 
	 * if we do calling loadUrl() to populate the WebView.
	 * if we do not have it, creating an intent to start the 
	 * DetailFragment Activity which does the similar thing
	 * (we have to pass the URL though).
	 */
	@Override
	public void onCountrySelected(Country c) {
		String url = getString(c.url);
		if(details!=null&&details.isVisible()){
			details.loadUrl(url);
		}else{
			Intent i = new Intent(this,DetailActivity.class);
			i.putExtra(DetailActivity.EXTRA_URL, url);
			startActivity(i);
		}
		
	}

	@Override
	public boolean isPersistentSelection() {
		return(details != null && details.isVisible());
	}
	
}
