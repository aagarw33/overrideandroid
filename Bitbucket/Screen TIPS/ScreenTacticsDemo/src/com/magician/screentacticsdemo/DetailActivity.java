package com.magician.screentacticsdemo;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * 
 * @author OverrideMAC
 *	creating detail fragment and loading it in the layout
 *  the URL is captured from the Intent extra and then 
 *  calling loadUrl().
 */
public class DetailActivity extends FragmentActivity{
	public static final String EXTRA_URL=
			"com.magician.screentacticsdemo.EXTRA_URL";
	private String url = null;
	private DetailFragment detailsFrag = null;
	
	@Override
	protected void onCreate(Bundle override) {
		super.onCreate(override);
		
		detailsFrag = 
				(DetailFragment)getFragmentManager().findFragmentById(R.id.details);
		
		if(detailsFrag == null){
			detailsFrag = new DetailFragment();
			getFragmentManager().beginTransaction()
					.add(android.R.id.content, detailsFrag)
					.commit();
		}
		url = getIntent().getStringExtra(EXTRA_URL);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		detailsFrag.loadUrl(url);
	}

}
