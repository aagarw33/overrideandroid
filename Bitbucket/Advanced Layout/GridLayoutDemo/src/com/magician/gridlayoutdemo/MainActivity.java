package com.magician.gridlayoutdemo;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ViewPager pager = (ViewPager)findViewById(R.id.pager);
		pager.setAdapter(buildAdapter());
	}

		private PagerAdapter buildAdapter(){
			return(new SampleAdapter(this,getFragmentManager()));
		}
}
