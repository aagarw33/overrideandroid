package com.magician.gridlayoutdemo;

/**
 * 
 * @author OverrideMAC
 *
 *         This class contains a pair of Layout ID and string resource ID for
 *         the fragment title.
 */
public class Sample {
	int mLayoutID;
	int mTitleID;

	Sample(int mLayoutID, int mTitleID) {
		this.mLayoutID = mLayoutID;
		this.mTitleID = mTitleID;
	}

}
