package com.magician.gridlayoutdemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 
 * @author OverrideMAC This class is fragment class that just inflates our
 *         layout
 */
public class TrivialFragment extends Fragment {

	private static final String LAYOUT_ID = "mLayoutID";

	static Fragment newInstance(int mLayoutID) {
		TrivialFragment frag = new TrivialFragment();
		Bundle args = new Bundle();
		args.putInt(LAYOUT_ID, mLayoutID);
		frag.setArguments(args);
		return (frag);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return (inflater.inflate(getArguments().getInt(LAYOUT_ID, -1),
				container, false));
	}

}
