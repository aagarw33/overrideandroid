/**
 * 
 */
package com.magician.gridlayoutdemo;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

/**
 * @author OverrideMAC
 *	This adapter maintains a static ArrayList of Sample objects.
 *	One per Layout, and uses these values to populate the ViewPager title.
 */
public class SampleAdapter extends FragmentPagerAdapter{
	
	static ArrayList<Sample> SAMPLE = new ArrayList<Sample>();
	private Context ctxt = null;
	
	static{
		SAMPLE.add(new Sample(R.layout.row, R.string.row));
		SAMPLE.add(new Sample(R.layout.column, R.string.column));
		SAMPLE.add(new Sample(R.layout.table,R.string.table));
		SAMPLE.add(new Sample(R.layout.table_flex, R.string.flexible_table));
		SAMPLE.add(new Sample(R.layout.implicit, R.string.implicit));
		SAMPLE.add(new Sample(R.layout.spans, R.string.spans));
	}
	
	public SampleAdapter(Context ctxt,FragmentManager manager){
		super(manager);
		this.ctxt = ctxt;
	}

	@Override
	public Fragment getItem(int position) {
		return(TrivialFragment.newInstance(getSample(position).mLayoutID));
	}
	
	

	@Override
	public String getPageTitle(int position) {
		return(ctxt.getString(getSample(position).mTitleID));
	}

	@Override
	public int getCount() {
		return(SAMPLE.size());
	}
	
	private Sample getSample(int position){
		return(SAMPLE.get(position));
	}
	
}
