package com.magician.downloadnotify;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class DownloadService extends IntentService {
	
	public static final String ACTION_COMPLETE
		="com.magician.downloadnotify.action.COMPLETE";
	private static int NOTIFY_ID =1337;
	
	public DownloadService(){
		super("DownloadService");
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}


	/*IntentService will stop itself automatically when there is no other request for downloads while onHandleIntent() is running
	
	 * onHandleIntent() is called on background thread.
	 * In this, we first set up a File object pointing to the location where we want to download the file. 
	 * If the directory does not exists on the location we create the directory using mkdirs().
	 * getLastPathSegment() method on Uri will return filename portion of path-style Uri.
	 * Then creating a typical HttpUrlConnection process to connect to the URL supplied via intent.
	 * 	 Finally sending the broadcast.
	 * */
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		try{
			File root = 
					Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			root.mkdirs();
			
			File output = new File(root,intent.getData().getLastPathSegment());
			
			if(output.exists()){
				output.delete();
			}
			
			URL url = new URL(intent.getData().toString());
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			
			FileOutputStream fos = new FileOutputStream(output.getPath());
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			
			try{
				InputStream in = con.getInputStream();
				byte[] buffer = new byte[8192];
				int len = 0;
				
				while((len = in.read(buffer)) >= 0){
					bos.write(buffer,0,len);
				}
				bos.flush();
				}finally{
					fos.getFD().sync();
					bos.close();
					con.disconnect();
			}
			raiseNotification(intent,output,null);
		}catch(IOException e){
			raiseNotification(intent,null,e);
		}

	}


	private void raiseNotification(Intent intent, File output, Exception e) {
		
		NotificationCompat.Builder b = new NotificationCompat.Builder(this);
		
		b.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL)
		.setWhen(System.currentTimeMillis());
		
		if(e==null){
			b.setContentTitle("Download Complete").setContentText("Test Fun")
			.setSmallIcon(android.R.drawable.stat_sys_download_done)
			.setTicker("Download Complete");
			
			Intent outbound = new Intent(Intent.ACTION_VIEW);
			outbound.setDataAndType(Uri.fromFile(output), intent.getType());
			b.setContentIntent(PendingIntent.getActivity(this, 0, outbound, 0));
		}else{
			b.setContentTitle("Exception").setContentText(e.getMessage())
			.setSmallIcon(android.R.drawable.stat_notify_error).setTicker("Exception");
		}
		
		NotificationManager mgr = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		mgr.notify(NOTIFY_ID, b.build());
	}
	

}
