package com.magician.bignotification;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

public class MainActivity extends Activity {

	private static final int NOTIFY_ID = 1337;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		NotificationManager mgr = 
				(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		NotificationCompat.Builder normal = buildNormal();
		NotificationCompat.InboxStyle big = new NotificationCompat.InboxStyle(normal);
		
		mgr.notify(NOTIFY_ID,
			big.setSummaryText("This is summary")
			.addLine("Entry")
			.addLine("Another Entry")
			.addLine("Third Entry")
			.addLine("Yet another entry")
			.addLine("Last Entry").build());
	finish();	
	}

	private NotificationCompat.Builder buildNormal(){
		NotificationCompat.Builder b = new NotificationCompat.Builder(this);
		b.setAutoCancel(true)
		.setDefaults(Notification.DEFAULT_ALL)
		.setWhen(System.currentTimeMillis())
		.setContentText("hello World")
		.setContentTitle("Bye world")
		.setContentIntent(buildPendingIntent(Settings.ACTION_SECURITY_SETTINGS))
		.setSmallIcon(android.R.drawable.stat_sys_download_done)
		.setTicker("Download Complete")
		.setPriority(Notification.PRIORITY_HIGH)
		.addAction(android.R.drawable.ic_media_play,
					"Play",buildPendingIntent(Settings.ACTION_SETTINGS));
		return(b);
		
	}

	  private PendingIntent buildPendingIntent(String action) {
		    Intent i=new Intent(action);

		    return(PendingIntent.getActivity(this, 0, i, 0));
		  }
}
