package com.magician.foregroundnotificationdemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DownloadFragment extends Fragment implements View.OnClickListener{

	private Button b = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_main, container,false);
		b = (Button)v.findViewById(R.id.button);
		b.setOnClickListener(this);
		return(v);
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(getActivity(),DownloadService.class);
		
		i.setDataAndType(Uri.parse("http://commonsware.com/Android/excerpt.pdf"),
                "application/pdf");
		getActivity().startService(i);
		getActivity().finish();
		
	}
	
	
	
}
