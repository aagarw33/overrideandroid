package com.magician.chronodemo;

/**
 * this class demonstrate the use of datepicker and timepicker
 * dailog fragments.
 */
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends Activity {
	TextView dateandTimeLabel;
	Calendar dateAndTime = Calendar.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		dateandTimeLabel = (TextView)findViewById(R.id.dateAndTime);
		updateLabel();
	}

	public void chooseDate(View v){
			new DatePickerDialog(this, d, 
					dateAndTime.get(Calendar.YEAR),
					dateAndTime.get(Calendar.MONTH), 
					dateAndTime.get(Calendar.DAY_OF_MONTH))
			.show();
	}
	
	public void chooseTime(View v){
		 new TimePickerDialog(this, t,
				 dateAndTime.get(Calendar.HOUR_OF_DAY),
				 dateAndTime.get(Calendar.MINUTE), true)
		 .show();
	}
	
	public void updateLabel(){
		dateandTimeLabel.setText(DateUtils
				.formatDateTime(this, dateAndTime.getTimeInMillis(),
						DateUtils.FORMAT_SHOW_DATE|DateUtils.FORMAT_SHOW_TIME));
	}
	
	DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			dateAndTime.set(Calendar.YEAR, year);
			dateAndTime.set(Calendar.MONTH, monthOfYear);
			dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}
	};
	
	TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateAndTime.set(Calendar.MINUTE, minute);
			updateLabel();
		}
	};
}
