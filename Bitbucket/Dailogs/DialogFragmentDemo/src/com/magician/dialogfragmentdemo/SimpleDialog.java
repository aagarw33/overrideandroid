/**
 * 
 */
package com.magician.dialogfragmentdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author OverrideMAC this class demonstrate the use of DialogFragment to
 *         construct a ALertDialog
 */
public class SimpleDialog extends DialogFragment implements
		DialogInterface.OnClickListener {
	private View form = null;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		form = getActivity().getLayoutInflater().inflate(R.layout.dialog, null);
		AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
		return (build.setTitle(R.string.dlg_title).setView(form)
				.setPositiveButton(android.R.string.ok, this)
				.setNegativeButton(android.R.string.cancel, null).create());
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		String temp = getActivity().getString(R.string.toast);
		EditText name = (EditText) form.findViewById(R.id.title);
		EditText value = (EditText) form.findViewById(R.id.value);
		String msg = String.format(temp, name.getText().toString(), value
				.getText().toString());
		Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		Toast.makeText(getActivity(), "GoodBye!", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		Toast.makeText(getActivity(), R.string.back, Toast.LENGTH_SHORT).show();
	}

}
