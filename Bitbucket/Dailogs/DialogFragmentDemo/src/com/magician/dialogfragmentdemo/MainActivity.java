package com.magician.dialogfragmentdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/*
 * this class demonstrate the use of DialogFragment to construct
 * a ALertDialog
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void showMe(View v){
		new SimpleDialog().show(getFragmentManager(), "simple");
	}

}
