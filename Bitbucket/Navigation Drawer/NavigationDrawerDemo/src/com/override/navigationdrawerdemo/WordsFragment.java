package com.override.navigationdrawerdemo;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class WordsFragment extends ListFragment {
	private static final String[] items= { "lorem", "ipsum", "dolor",
	      "sit", "amet", "consectetuer", "adipiscing", "elit", "morbi",
	      "vel", "ligula", "vitae", "arcu", "aliquet", "mollis", "etiam",
	      "vel", "erat", "placerat", "ante", "porttitor", "sodales",
	      "pellentesque", "augue", "purus" };
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setListAdapter(new ArrayAdapter<String>(getActivity(),
							android.R.layout.simple_list_item_1,
							items));
	}
	
}
