package com.override.navigationdraweractivateddemo;

import android.app.Activity;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * 
 * @author OverrideMAC
 *
 *	THis project is clone implementation of Simple Navigation Drawer.
 *	Here the list item is activated if the user is on the same page.
 */
public class MainActivity extends Activity 
			implements OnItemClickListener,WordsFragment.Contract,
			OnBackStackChangedListener{

	private DrawerLayout drawNav = null;
	private ActionBarDrawerToggle toggle = null;
	private WordsFragment words = null;
	private ContentFragment content = null;
	private static final String STATE_CHECKED = 
			"com.override.navigationdraweractivateddemo.STATE_CHECKED";
	private ListView drawer = null;
	private StuffFragment stuff = null;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		drawer = (ListView)findViewById(R.id.drawer);
		drawer.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		String[] rows = getResources().getStringArray(R.array.drawer_rows);
	
		// sets the contents of list to be of <string-array>
		drawer.setAdapter(new ArrayAdapter<String>(this,
					R.layout.drawer_row,rows));
		
		drawer.setOnItemClickListener(this);
		
		drawNav = (DrawerLayout)findViewById(R.id.drawer_layout);
		/* setting up actionbardrawertoggle.
		 * this allows app icon to open and close the drawer
		 * Parameters:
		 * 		-	activity context
		 * 		-	drawerlayout widge to be managed
		 * 		-	icon to superimpose on app icon to indicate that there is toggle
		 * 		-	string resources for open and close operations
		 */
		toggle =
				new ActionBarDrawerToggle(this, drawNav, android.R.drawable.btn_plus, 
										R.string.drawer_open, R.string.drawer_close);
		drawNav.setDrawerListener(toggle);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getFragmentManager().addOnBackStackChangedListener(this);
		if(getFragmentManager().findFragmentById(R.id.content) == null){
			showWords();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if(position == 0){
			showWords();
		}else{
			showContent();
		}
		
		drawNav.closeDrawers();
		
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		toggle.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		toggle.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(toggle.onOptionsItemSelected(item)){
			return(true);
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void showWords(){
		if(words == null){
			words = new WordsFragment();
		}
		
		if(!words.isVisible()){
			getFragmentManager().beginTransaction()
								.replace(R.id.content, words).commit();
		}
		drawer.post(onNavChange);
	}
	
	private void showContent(){
		if(content == null){
			content = new ContentFragment();
		}
		
		if(!content.isVisible()){
			getFragmentManager().beginTransaction()
								.replace(R.id.content, content).commit();
		}
		drawer.post(onNavChange);
	}

	@Override
	public void onBackStackChanged() {
		drawer.post(onNavChange);
	}

	@Override
	public void wordClicked() {
		if(stuff == null){
			stuff = new StuffFragment();
		}
		
		getFragmentManager().beginTransaction()
		.replace(R.id.content, stuff)
		.addToBackStack(null).commit();
	drawer.post(onNavChange);
	}
	
	
	/*
	 * this simply sees what fragment is presently visible
	 * and updates the checked item in the nav drawer's ListView to match.
	 */
	private Runnable onNavChange=new Runnable() {
		    @Override
		    public void run() {
		      if (words != null && words.isVisible()) {
		        drawer.setItemChecked(0, true);
		      }
		      else if (content != null && content.isVisible()) {
		        drawer.setItemChecked(1, true);
		      }
		      else {
		        int toClear=drawer.getCheckedItemPosition();

		        if (toClear >= 0) {
		          drawer.setItemChecked(toClear, false);
		        }
		      }
		    }
		 };
	
}
