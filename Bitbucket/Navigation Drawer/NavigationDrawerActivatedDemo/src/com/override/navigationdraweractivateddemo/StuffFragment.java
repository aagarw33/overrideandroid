package com.override.navigationdraweractivateddemo;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
 * this fragment is not is navigation drawer list
 */
public class StuffFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View res = inflater.inflate(R.layout.stuff, container,false);
		return(res);
	}
}
